from queue import Queue
from Utils import globalVar as gv
from Utils.sheetUtils import update_sheet_by_df
from check_books import CheckBooks
from utilities import get_verticalwise_big_book_topics, setup_gv, get_all_queue_result
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
import random, time
import sheets_list
from Utils.update_results import ResultsUpdater
import sys



def async_sheet_updater():
    global report
    global kill_updater
    global results_updater
    while True:
        time.sleep(300)
        _report = get_all_queue_result(report)
        results_updater.update_in_db(_report)
        try:
            df = pd.DataFrame(_report)
        except:
            print(f"Error in updating; report size {len(_report)}!!!")
            continue
        df.to_csv("Results/" + "check_book_ops_report.csv")
        if gv.call_type == "full":
            update_sheet_by_df(sheets_list.check_books_sheets.get(gv.local_to_check), df, gv.local_to_check)
        print(f"Updating {len(_report)} ids at {time.time()}!!!!")
        if kill_updater:
            break
    return

def async_run(topic, report):
    report.put(CheckBooks().check_book_ops(topic))

def main(report_list):
    global kill_updater, results_updater
    
    try:
        shutil.rmtree("Results")
    except:
        pass
    try:
        os.mkdir("Results")
    except:
        pass
    topic_list = CheckBooks.get_subjects_list()
    # topic_list = random.choices(topic_list, k = 10)
    sheet_update_thread = Thread(target = async_sheet_updater)
    # with ThreadPoolExecutor(max_workers = 50) as executor:
    #     sheet_update_thread.start()
    #     executor.map(async_run, topic_list, [report]*len(topic_list))

    for topic in topic_list:
        async_run(topic, report)

    kill_updater = True
    # report_df = pd.DataFrame(report_list)
    # report_df.to_csv("Results/check_book_ops_report.csv")
    results_updater.update_in_db(get_all_queue_result(report))
    print("updating report meta")
    results_updater.mark_as_complete()
    print("report meta updated")
    sys.exit()
    # if gv.call_type == "full":
    #     update_sheet_by_df(sheets_list.check_books_sheets.get(gv.local_to_check),  report_df, gv.local_to_check)

if __name__ == "__main__":
    setup_gv()
    report = Queue()
    results_updater = ResultsUpdater("check_books", gv.local_to_check)
    kill_updater = False
    main(report)