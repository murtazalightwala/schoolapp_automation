import requests
import lxml
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
import time
from datetime import date, datetime
from Utils.email_utils import Emailer
from Utils.reportUtils import SEOWarmupReport
from Utils import globalVar as gv

def get_sitemaps():
    for i in range(3):
        sitemap = requests.get("https://www.embibe.com/static-files/sitemap.xml")
        if sitemap.status_code == 200:
            sitemap_soup = BeautifulSoup(sitemap.content, "lxml")
            return [x.loc.text for x in sitemap_soup.find_all("sitemap")]

def get_urls_from_sitemap(sitemap_url):
    for i in range(3):
        sitemap_response = requests.get(sitemap_url)
        if sitemap_response.status_code == 200:
            sitemap_soup = BeautifulSoup(sitemap_response.content, "lxml")
            return [x.loc.text for x in sitemap_soup.find_all("url")]

def async_data_updater():
    global failed_results
    global kill_updater, all_urls, current_url, start_updates
    while True:
        
        _report = failed_results
        time.sleep(60)
        if not start_updates:
            print("Not updating")
            continue
        try:
            df = pd.DataFrame(_report)
            df.to_csv("temp_warmup_results.csv")
            # email_status(df, all_urls.index(current_url), len(all_urls))
        except Exception as e:
            print(f"Error in updating; results size {len(_report)}!!!", e)
            continue
        if kill_updater:
            break
    return

def check_url(url):
    # print(url)
    global failed_results
    global current_url
    current_url = url
    response = requests.get(url)
    if response.status_code == 200:
        return
    failed_results.append({"url":url, "status code": response.status_code})
    
def main():
    gv.processSysArgs()
    global all_urls, current_url, start_updates
    data_update_thread = Thread(target = async_data_updater, name = "Async Updater")
    data_update_thread.start()
    all_sitemaps = get_sitemaps()
    all_urls = []
    for sitemap in all_sitemaps:
        all_urls.extend(get_urls_from_sitemap(sitemap))
    skip = gv.skip
    start_updates = True
    with ThreadPoolExecutor(max_workers=100) as ex:
        ex.map(check_url, all_urls[skip:])
        # for url in all_urls:
        #     check_url(url)
    return 

def email_status(results_df, count, number_of_urls):
    emailer = Emailer()
    emailer.set_sender_email("murtaza.lightwala@embibe.com")
    emailer.set_to_emails(["murtaza.lightwala@embibe.com"])
    emailer.set_sender_password("kgrlapilipvqnsll")
    emailer.add_html(
        f"""
        <p>Hi,
        </p>
        <p>
PFA the status for SEO Warmup at {date.today().strftime("%d-%m-%Y")}
# Done urls = {count}
# Urls = {number_of_urls}

</p>
""")
    if len(results_df) != 0:
        report = SEOWarmupReport(results_df)
        emailer.add_html(report.create_report_html())
    emailer.set_subject("SEO Warmup report")
    emailer.add_attachments("warmup_report.csv", "temp_results.csv")
    emailer.send_email()



def email_report(results_df):
    emailer = Emailer()
    report = SEOWarmupReport(results_df)
    emailer.set_sender_email("murtaza.lightwala@embibe.com")
    emailer.set_to_emails(["murtaza.lightwala@embibe.com"])
    emailer.set_sender_password("kgrlapilipvqnsll")
    emailer.add_html(
        f"""
        <p>Hi,
        </p>
        <p>
PFA the output for SEO Warmup on {date.today().strftime("%d-%m-%Y")}
</p>
""")
    emailer.add_html(report.create_report_html())
    emailer.set_subject("SEO Warmup report")
    emailer.add_attachments("warmup_report.csv", "results.csv")
    

if __name__ == "__main__":
    kill_updater = False
    start_updates = False
    failed_results = []
    main()
    results_df = pd.DataFrame(failed_results)
    results_df.to_csv("warmup_results.csv")
    kill_updater = True
    # email_report(results_df)
