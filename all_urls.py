assign_homework_urls = {
    "create": "/ps_generate_ms/v3/school/atgConfig?locale={}",
    "get_syllabus": "/ps_generate_ms/v3/school/syllabus?atgConfigId={}&locale={}",
    "get_chapter_topics": "/ps_generate_ms/v3/school/chapterTopics?atgConfigId={}&chapterKveCode={}&locale={}",
    "get_books_content": "/ps_generate_ms/v3/school/booksContent?atgConfigId={}&size=20&offset=1&locale={}",
    "set_activity_content": "/ps_generate_ms/v3/school/activityContent?atgConfigId={}&locale={}",
    "confirm_syllabus": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "get_homework_chapters": "/ps_generate_ms/v3/school/homeworkChapters?atgConfigId={}&locale={}",
    "get_homework_topics": "/ps_generate_ms/v3/school/homeworkTopics?atgConfigId={}&chapterKveCode={}&locale={}",
    "get_topic_videos": "/ps_generate_ms/v3/school/homeworkVideos?atgConfigId={}&topicKveCode={}&offset=1&size=10&locale={}",
    "get_topic_questions": "/ps_generate_ms/v3/school/topicQuestions?atgConfigId={}&topicKveCode={}&offset=1&size=10&locale={}",
    "fiber_ms_book": "/fiber_ms/book?book_id={}&type=NormalBook&locale={}&exam_code={}&goal_code={}",
    "fiber_app_lmf": "/cg-fiber-ms/fiber_app/learning_maps/filtersv2/{}?locale={}",
    "set_videos": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "set_questions": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "get_all_tests": "/ps_generate_ms/v3/school/getAllTest?atgConfigId={}&locale={}",
    "get_all_chapters": "/ps_generate_ms/v3/school/chapters?atgConfigId={}&locale={}",
    "select_test": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "get_all_test_questions": "/ps_generate_ms/v3/school/getAllQuestions?atgConfigId={}&offset=1&size=10&locale={}",
    "review_questions": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "test_schedule": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "set_atg_meta": "/ps_generate_ms/v3/school/atgConfig/{}?locale={}",
    "publish": "/ps_generate_ms/v3/school/publish?atgConfigId={}&locale={}"
}

prerequisite_readiness_url = "/track/v1/prerequisiteReadiness?locale={}"
student_prerequisite_url = "/fiber_ms/topic/prerequisites"
student_topic_videos_url = "/fiber_ms/v1/topic/learning-objects?&learnMapId={}&contentTypes=Video&locale={}"
recap_suggestions_url = "/track/v1/recapSuggestions?locale={}"
fs_ms_search_url = "/fs_ms/v2/search"
fs_ms_ampsearch_url = "/fs_ms/v2/ampsearch?size=100&start=0&user_id={}&locale={}&query=*&filter=meta_kve_codes.keyword%3A{}&{}"
coobo_ms_recommended_lessons = "/coobo_ms/recommended-lessons"
coobo_ms_phets = "/coobo_ms/phets"
prebuilt_lessons_url = "/coobo_ms/lessons"
