from cProfile import run
import shutil, os
from Utils import globalVar as gv
from Utils.sheetUtils import update_sheet_by_df
from assign import HomeworkAssigner
from utilities import get_verticalwise_book_topics, setup_gv, get_failed_chapter_topics
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
from sheets_list import results_sheets
import random, time



def main():
    global kill_updater
    setup_gv()
    print(gv.class_id_list)
    

if __name__ == "__main__":
    report = []
    kill_updater = False
    main()