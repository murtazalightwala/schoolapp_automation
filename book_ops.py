import Utils.globalVar as gv
import json
from Utils.callAPI import callAPI
from responsereaders import BookResponseReader, BookOpsResponseReader, LearningMapsFormatsResponseReader


class BookOps:
    def __init__(self):
        pass


    def _book_ops(self, book_id, topic, type, class_id, change):
        book_ops_payload = [
    {
        "classOrgId": class_id,
        "bookType": type,
        "id": book_id,
        "subjectLMReference": topic.get("subject_id"),
        "status": change
    }
    ]
        book_ops_response = callAPI(f"/narad/v2/teacher/class/subjects/bookOps", payload= book_ops_payload, method="PUT", params = {"locale": gv.local_to_check})
        return book_ops_response

    def add_book(self, book_id, topic, type, class_id):
        book_ops_response = self._book_ops(book_id, topic, type, class_id, "Active")
        book_ops_response_reader = BookOpsResponseReader(book_ops_response)
        if book_ops_response_reader.is_updated():
            raise Exception(book_ops_response_reader.get_error_message())
        return book_ops_response_reader.get_confirmation()
        

    def remove_book(self, book_id, topic, type, class_id):
        book_ops_response = self._book_ops(book_id, topic, type, class_id, "Deleted")
        book_ops_response_reader = BookOpsResponseReader(book_ops_response)
        if not book_ops_response_reader.is_updated():
            raise Exception(book_ops_response_reader.get_error_message())
        return book_ops_response_reader.get_confirmation()

    def list_all_books_on_school_app(self, grade, subject, only_ids = False):
        payload =  {
    "grade": grade,
    "pageNumber": 1,
    "pageSize": 1000,
    "subject": subject,
    "locale": gv.local_to_check
            }
        
        books_response = callAPI(f"/cg-fiber-ms/school/books", payload = payload, method = "POST", params = {"locale": gv.local_to_check})
        big_books_response = callAPI(f"/cg-fiber-ms/school/big_books", payload = payload, method = "POST", params = {"locale": gv.local_to_check})
        books_response_reader = BookResponseReader(books_response)
        big_books_response_reader = BookResponseReader(big_books_response)
        book_list = []
        book_list.extend(books_response_reader.get_book_list("Book"))
        book_list.extend(big_books_response_reader.get_book_list("BigBook"))
        if only_ids:
            return [x.get("id") for x in book_list]
        return book_list

    def list_all_books_in_learning_maps_formats(self, topic, only_ids = False):
        query = {"content.grade": topic.get("grade"), 
        f"locales.{gv.local_to_check}.content.subjects": topic.get("subject_name"),
         f"locales.{gv.local_to_check}.status": "active"}
        
        params = {
            "where": json.dumps(query),
            "locale": gv.local_to_check,
            "max_results": 100
        }
        response = callAPI("/cg-fiber-ms/learning_map_formats", payload={}, method = "GET", params = params)
        lmf_response_reader = LearningMapsFormatsResponseReader(response)
        book_list = lmf_response_reader.get_book_list()
        if only_ids:
            return [x.get("content").get("book_code") for x in book_list]
        return book_list
    



        
    
