from book_ops import BookOps
from Utils.callAPI import callAPI
from Utils import globalVar as gv
from responsereaders import SubjectsResponseReader
from book_ops import BookOps




class CheckBooks():
    def __init__(self) -> None:
        pass

    def get_difference(school_app_book_list, lmf_book_list):
        sdl = []
        lds = []
        school_app_id_list = [x.get("id") for x in school_app_book_list]
        lmf_id_list = [x.get("content", {}).get("book_code") for x in lmf_book_list]
        sdl = list(set(school_app_id_list) - set(lmf_id_list))
        lds = list(set(lmf_id_list) - set(school_app_id_list))
        return sdl, lds
    
    def get_union(school_app_book_list, lmf_book_list):
        book_list = []
        for book in school_app_book_list:
            _book = {
                "id": book.get("id"),
                "type": book.get("type")
            }
            book_list.append(_book)
        for book in lmf_book_list:
            _book = {
                "id": book.get("content").get("book_code"),
                "type": "Book" if book.get("type") == "book" else "BigBook"
            }
            if _book.get("id") not in [x.get("id") for x in book_list]:
                book_list.append(_book)
        return book_list
    
    def remove_all_books(topic):
        book_list = topic.get("book_list")
        big_book_list = topic.get("big_book_list")
        for book in book_list:
            BookOps().remove_book(book, topic, "Book", topic.get("class_org_id"))
        for book in big_book_list:
            BookOps().remove_book(book, topic, "BigBook", topic.get("class_org_id"))
        print("All Books removed")
        return

    def get_subjects_list():
        response = callAPI("/narad/v2/teacher/class/subjects", payload={}, method = "GET", params = {"locale": gv.local_to_check})
        subjects_response_reader = SubjectsResponseReader(response)
        return subjects_response_reader.get_all_subjects()

    def check_book_ops(self, topic):
        book_ops = BookOps()
        topic_results = {
            "goal": topic.get("board"),
            "exam": topic.get("grade"),
            "subject": topic.get("subject_name")
            }
            
        school_app_book_list = book_ops.list_all_books_on_school_app(topic.get("grade"), topic.get("subject_name"))
        lmf_book_list = book_ops.list_all_books_in_learning_maps_formats(topic)
        topic_results["# Books in School App"] = len(school_app_book_list)
        topic_results["# Books in CG"] = len(lmf_book_list)
        books_not_in_cg, books_not_in_school_app = __class__.get_difference(school_app_book_list, lmf_book_list)
        topic_results["Books not in CG"] = books_not_in_cg
        topic_results["Books not in School App"] = books_not_in_school_app
        books_not_added = []
        books_not_removed = []
        for book in __class__.get_union(school_app_book_list, lmf_book_list):
            try:
                book_ops.add_book(book.get("id"), topic, book.get("type"), topic.get("class_org_id"))
            except Exception as e:
                books_not_added.append(book.get("id"))
                print(str(e))
            try:
                book_ops.remove_book(book.get("id"), topic, book.get("type"), topic.get("class_org_id"))
            except Exception as e:
                books_not_removed.append(book.get("id"))
                print(str(e))
        topic_results["Books not added"] = books_not_added
        topic_results["Books not removed"] = books_not_removed
        return topic_results




        
        
