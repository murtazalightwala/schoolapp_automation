from Utils import globalVar as gv
from Utils.callAPI import callAPI
from responsereaders import RecapSuggestionsResponseReader
from all_urls import recap_suggestions_url

class RecapSuggestions():

    def __init__(self, locale, headers, get_class_id) -> None:
        self.locale = locale
        self.headers = headers
        self.get_class_id = get_class_id
        self.headers["embibe-token"] = headers["reseller-jwt-token"]
        return

    def get_recap_suggestions(self, topic):
        report = {}
        report["topic_meta"] = topic
        report["topic_id"] = topic["exam_code"] + "--" + topic["subject_code"] + "--" + topic["topic_code"]+ "--"+ self.locale
        report["Goal"] = topic.get("goal")
        report["Exam"] = topic.get("exam")
        report["Book"] = topic.get("book_name", "Embibe Big Book")
        report["Vertical"] = topic.get("vertical")
        report["Chapter"] = topic.get("chapter_name")
        report["Topic"] = topic.get("topic_name")

        payload = {
            "classId": self.get_class_id(topic),
            "kvCode": topic.get("subject_kv_code_slug"),
            "slotId": "6222034009a8894bfaafa810",
            "isLiveClass": False
        }
        response = callAPI(url = recap_suggestions_url.format(self.locale), payload = payload, method = "POST", headers = self.headers)
        recap_suggestions_response_reader = RecapSuggestionsResponseReader(response)
        
        report["API status code"] = recap_suggestions_response_reader.get_status_code()
        return report
