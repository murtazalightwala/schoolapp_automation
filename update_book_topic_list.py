import shutil, os
from Utils import globalVar as gv
from sheets_list import book_topic_sheets
from Utils.sheetUtils import update_sheet_by_df
from Utils.db import QADb
from get_chapters import get_topic_list, get_big_book_topic_list
from utilities import setup_gv
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
import random, time

def update_topics_in_db(all_topics):
    db = QADb()
    collection = db.topic_list
    book_topics_df = pd.DataFrame(all_topics)
    book_topics_df = book_topics_df[['format_ref', 'goal', 'goal_code', 'exam', 'exam_code', 'vertical',
       'subject', 'subject_code', 'book_name', 'path', 'chapter_name',
       'chapter_code', 'topic_name', 'topic_display_name', 'chapter_kve_code',
       'topic_code', 'subject_id', 'topic_kve_code', 'topic_learnpath']]
    book_topics_df["d"] = book_topics_df.apply(lambda x: "--".join([x["goal_code"], x["exam_code"], x["subject_code"], x["chapter_code"], x["topic_code"]]).lower(), axis = 1)
    g = book_topics_df.groupby("d")
    df = g.sample(1)
    df.drop(columns = ["d"], inplace = True)
    df["type"] = "book"
    df["locale"] = gv.local_to_check
    df["_id"] = df.apply(lambda x: x["exam_code"] + "--" + x["subject_code"] + "--" + x["topic_code"]+ "--"+ x["locale"], axis = 1)
    li = df.to_dict("records")
    print(len(li))
    try:
        collection.delete_many({"_id": {"$in":[x.get("_id") for x in li]}})
        collection.insert_many(li, ordered = False)
    except:
        while True:
            temp_li = []
            for i in range(100000):
                if len(li)>0:
                    temp_li.append(li.pop())
                else:
                    collection.delete_many({"_id": {"$in":[x.get("_id") for x in temp_li]}})
                    collection.insert_many(li, ordered = False)
                    break
            else:
                collection.delete_many({"_id": {"$in":[x.get("_id") for x in temp_li]}})
                collection.insert_many(li, ordered = False)
                continue
            break




def main(report_list):
    setup_gv()
    all_topics = get_topic_list()
    all_topics.extend(get_big_book_topic_list())
    update_topics_in_db(all_topics)

if __name__ == "__main__":
    report = []
    kill_updater = False
    main(report)