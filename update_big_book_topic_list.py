from Utils import globalVar as gv
from sheets_list import big_book_topics_sheets
from Utils.sheetUtils import update_sheet_by_df
from Utils.db import QADb
from get_chapters import get_big_book_topic_list
from utilities import setup_gv
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
import random, time


def update_topics_in_db(all_topics):
    db = QADb()
    collection = db.topic_list
    book_topics_df = pd.DataFrame(all_topics)
    book_topics_df = book_topics_df[['format_ref', 'goal', 'vertical', 'goal_code', 'exam', 'exam_code',
       'subject', 'subject_code', 'chapter_name', 'chapter_code', 'topic_name',
       'topic_display_name', 'topic_code', 'learnpath_name']]
    book_topics_df["d"] = book_topics_df.apply(lambda x: "--".join([x["goal_code"], x["exam_code"], x["subject_code"], x["chapter_code"], x["topic_code"]]).lower(), axis = 1)
    g = book_topics_df.groupby("d")
    df = g.sample(1)
    df.drop(columns = ["d"], inplace = True)
    df["type"] = "big_book"
    df["locale"] = gv.local_to_check
    df["_id"] = df.apply(lambda x: x["exam_code"] + "--" + x["subject_code"] + "--" + x["topic_code"]+ "--"+ x["locale"], axis = 1)
    li = df.to_dict("records")
    print(len(li))
    collection.delete_many({"_id": {"$in":[x.get("_id") for x in li]}})
    collection.insert_many(li, ordered = False)

def main(report_list):
    setup_gv()
    all_topics = get_big_book_topic_list()
    topics_df = pd.DataFrame(all_topics)
    update_topics_in_db(all_topics)
    update_sheet_by_df(big_book_topics_sheets.get(gv.local_to_check), topics_df, "Topics")

if __name__ == "__main__":
    report = []
    kill_updater = False
    main(report)