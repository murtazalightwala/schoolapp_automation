import pandas as pd
import requests
import json
import copy
import Utils.globalVar as gv
from responsereaders import FiberCountriesResponseReader
from Utils.callAPI import callAPI

def get_goal_exams():
    url = f"/content_ms_fiber/v1/embibe/{gv.local_to_check}/fiber-countries-goals-exams"
    response = callAPI(url = url, payload = {}, method = "GET")
    fiber_response_reader = FiberCountriesResponseReader(response)
    return fiber_response_reader.get_all_goal_exam_locales()
    # return [x for x in fiber_response_reader.get_all_goal_exam_locales() if "Mahendra" in x.get("goal_name")]

def get_all_subjects(id_):
    url = f"https://preprodms.embibe.com//cg-fiber-ms/fiber_app/learning_maps/filters/{id_}?locale={gv.local_to_check}"
    payload={}
    headers = {
      'accept': '*/*',
      'embibe-token': gv.embibe_token,
      'Content-Type': 'application/json',
      'Cookie': 'V_ID=ultimate.2021-08-23.a2718262528a0e85f1c04f0de66362a7; embibe-refresh-token=563b933f-1f58-48ff-87a0-ed8997655e04; preprod_embibe-refresh-token=66b3fb33-5d6f-48f5-a5c1-ae7f2cd53e53; prod_ab_version=0; prod_embibe-refresh-token=44e0b005-60f4-41f9-9559-0e1ba2c5006d; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NDk4MzYyOTcsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MjAwNDM4Mjc5NSwiZXhwIjoxNjUxMDQ1ODk3LCJkZXZpY2VJZCI6IjAxMjM0NTY4LTg5Q0JDREVHLTAwMjg0NTYwLTgwQUJDRE1GIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJhbGl5YS50ZXN0aW5nQGdtYWlsLmNvbSJ9.6cCRaUcvalCHff2o5NtjrMmw8VIEqKnDR0lzbVS3wC5XIuVK5pRfcAy3ENRbMGtsw99cQTahxJDeFLkQMMmpLQ; prod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1VlhBaUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lKbU9HSmxZbU5qT1Mxak16aGpMVFJrTmpRdE9HVTRZeTAyTkROaVpHWmhZelExTmpFaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNkltTTJaamRqTlRCbUxXTmhOV010TkRKbU1pMWlOamxqTFRGalkyTmhOREZqTUdRek1DSXNJbWxrSWpvaVlXeHBlV0V1ZEdWemRHbHVaMEJuYldGcGJDNWpiMjBpTENKc2IyTmhiR1VpT2lKbGJpSjkuUjVPNTY0SlJKbzVwRDNqektuUEp6RFVwUGliVkFFTDJaRUFUYW9Ra01yZXFfU2F6SjA3anhXdW1uY2JRNlhwd3lEcTNNTXNiTTZqNE90amdTdUVVcXciLCJjcmVhdGVkIjoxNjQ5ODM2Mjk3LCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduVXAiLCJhdHRlbXB0IjowLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiYWxpeWEudGVzdGluZ0BnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoyMDA0MzgyNzk1LCJleHBpcnkiOjE2NDk5MjI2OTcsImV4cCI6MTY0OTkyMjY5N30.wpbrmG9b09kxNyfj1PvJolTQjVaOIssSamtWFisAt7Za_R0F-Pln3_TtIlEOQHCSk9eVjQnvVoBO7PJ7757JNQ; school_preprod_embibe-refresh-token=1aca3e5a-e474-45eb-ada6-e89945acdce4'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code != 200:
        return []
    subject_list=[]
    for subject in response.json().get("Subject",[]):
        subject_list.append([subject.get("display_name"),subject.get("code"), subject.get("name") ])
    return subject_list

def get_next_level(id_):
    host="https://preprodms.embibe.com"
    url = f"/cg-fiber-ms/fiber_app/learning_maps/filters/{id_}?locale={gv.local_to_check}&school=true"
    payload={}
    headers = {
    'authority': 'preprodms-cf.embibe.com',
    'accept': '*/*',
    'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'authorization': '048f38be-1b07-4b21-8f24-eac727dce217:gSEkC3dqDcIv1bbOk78UD9owjn7ins8D',
    'embibe-token': gv.embibe_token,
    'origin': 'https://staging-fiber-web.embibe.com',
    'referer': 'https://staging-fiber-web.embibe.com/',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36',
    'Cookie': 'V_ID=ultimate.2021-08-23.a2718262528a0e85f1c04f0de66362a7; embibe-refresh-token=563b933f-1f58-48ff-87a0-ed8997655e04; preprod_embibe-refresh-token=66b3fb33-5d6f-48f5-a5c1-ae7f2cd53e53; prod_ab_version=0; prod_embibe-refresh-token=44e0b005-60f4-41f9-9559-0e1ba2c5006d; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NDk4MzYyOTcsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MjAwNDM4Mjc5NSwiZXhwIjoxNjUxMDQ1ODk3LCJkZXZpY2VJZCI6IjAxMjM0NTY4LTg5Q0JDREVHLTAwMjg0NTYwLTgwQUJDRE1GIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJhbGl5YS50ZXN0aW5nQGdtYWlsLmNvbSJ9.6cCRaUcvalCHff2o5NtjrMmw8VIEqKnDR0lzbVS3wC5XIuVK5pRfcAy3ENRbMGtsw99cQTahxJDeFLkQMMmpLQ; prod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1VlhBaUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lKbU9HSmxZbU5qT1Mxak16aGpMVFJrTmpRdE9HVTRZeTAyTkROaVpHWmhZelExTmpFaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNkltTTJaamRqTlRCbUxXTmhOV010TkRKbU1pMWlOamxqTFRGalkyTmhOREZqTUdRek1DSXNJbWxrSWpvaVlXeHBlV0V1ZEdWemRHbHVaMEJuYldGcGJDNWpiMjBpTENKc2IyTmhiR1VpT2lKbGJpSjkuUjVPNTY0SlJKbzVwRDNqektuUEp6RFVwUGliVkFFTDJaRUFUYW9Ra01yZXFfU2F6SjA3anhXdW1uY2JRNlhwd3lEcTNNTXNiTTZqNE90amdTdUVVcXciLCJjcmVhdGVkIjoxNjQ5ODM2Mjk3LCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduVXAiLCJhdHRlbXB0IjowLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiYWxpeWEudGVzdGluZ0BnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoyMDA0MzgyNzk1LCJleHBpcnkiOjE2NDk5MjI2OTcsImV4cCI6MTY0OTkyMjY5N30.wpbrmG9b09kxNyfj1PvJolTQjVaOIssSamtWFisAt7Za_R0F-Pln3_TtIlEOQHCSk9eVjQnvVoBO7PJ7757JNQ; school_preprod_embibe-refresh-token=1aca3e5a-e474-45eb-ada6-e89945acdce4'
    }

    response = requests.request("GET", host+url, headers=headers, data=payload)
    
    return response

def get_all_books(exam,subject):
    host="https://preprodms.embibe.com"
    url = f"/cg-fiber-ms/learning_map_formats?where={{\"content.grade\": \"{exam}\", \"locales.{gv.local_to_check}.content.subjects\": \"{subject}\", \"locales.{gv.local_to_check}.status\": \"active\"}}&locale={gv.local_to_check}&max_results=80&page=1&school=false"
    payload={}
    headers = {
      'Accept': 'application/json, text/plain, */*',
      'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
      'Connection': 'keep-alive',
      'Origin': 'https://paas-v3-staging.embibe.com',
      'Referer': 'https://paas-v3-staging.embibe.com/',
      'Sec-Fetch-Dest': 'empty',
      'Sec-Fetch-Mode': 'cors',
      'Sec-Fetch-Site': 'same-site',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36',
      'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
      'sec-ch-ua-mobile': '?0',
      'sec-ch-ua-platform': '"macOS"',
      'Cookie': 'V_ID=ultimate.2021-08-23.a2718262528a0e85f1c04f0de66362a7; embibe-refresh-token=563b933f-1f58-48ff-87a0-ed8997655e04; preprod_ab_version=0; preprod_embibe-refresh-token=66b3fb33-5d6f-48f5-a5c1-ae7f2cd53e53; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NDk4MzYyNzQsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMTY0OTAxMCwiZXhwIjoxNjQ5OTIyNjc0LCJkZXZpY2VJZCI6IjAxMjM0NTY4LTg5Q0JDREVHLTAwMjg0NTYwLTgwQUJDRE1GIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJhbGl5YS50ZXN0aW5nQGdtYWlsLmNvbSJ9.LXjkxBO2Iq8ceqbD0D1AeHNc8UVnytOm6Oq1VVEdrv1bFnrr19NdQxBluLcr21rGsN33xVsQ8pNXEOxWgl1UoQ; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1VlhBaUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lKaVpEQXdORGN5TVMwd05tUm1MVFEzWmpVdFlqSXlNaTB4Tm1OaVl6WXdOMlkyTURRaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNklqVmxZV0kzT0dZM0xURmlaR010TkRRd1pTMWlaRGsyTFdNM05qSXpPRGs0TVdFeU9TSXNJbWxrSWpvaVlXeHBlV0V1ZEdWemRHbHVaMEJuYldGcGJDNWpiMjBpTENKc2IyTmhiR1VpT2lKbGJpSjkuRUgwUTFuT3Z3OEtRRUxRNVk4QWtOZ2lBS253TFdSal9TWmhLMm44Sm01WUZkdzY1SUI2SXZFM2RuWDNWQW42WkRFVDN1UTBOa2duNDlrbzczVFlKVkEiLCJjcmVhdGVkIjoxNjQ5ODM2Mjc0LCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduVXAiLCJhdHRlbXB0IjowLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiYWxpeWEudGVzdGluZ0BnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAxNjQ5MDEwLCJleHBpcnkiOjE2NDk5MjI2NzQsImV4cCI6MTY0OTkyMjY3NH0.rPeC3fUztDoVzu4kVWARD4CBbfdl5o9w00ympTRCj7tHCVPha1VCR2B4eN31hJeetX0LISYsTaqVj6RpV5BaHQ; prod_ab_version=0; prod_embibe-refresh-token=44e0b005-60f4-41f9-9559-0e1ba2c5006d; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NDk4MzYyOTcsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MjAwNDM4Mjc5NSwiZXhwIjoxNjUxMDQ1ODk3LCJkZXZpY2VJZCI6IjAxMjM0NTY4LTg5Q0JDREVHLTAwMjg0NTYwLTgwQUJDRE1GIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJhbGl5YS50ZXN0aW5nQGdtYWlsLmNvbSJ9.6cCRaUcvalCHff2o5NtjrMmw8VIEqKnDR0lzbVS3wC5XIuVK5pRfcAy3ENRbMGtsw99cQTahxJDeFLkQMMmpLQ; prod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1VlhBaUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lKbU9HSmxZbU5qT1Mxak16aGpMVFJrTmpRdE9HVTRZeTAyTkROaVpHWmhZelExTmpFaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNkltTTJaamRqTlRCbUxXTmhOV010TkRKbU1pMWlOamxqTFRGalkyTmhOREZqTUdRek1DSXNJbWxrSWpvaVlXeHBlV0V1ZEdWemRHbHVaMEJuYldGcGJDNWpiMjBpTENKc2IyTmhiR1VpT2lKbGJpSjkuUjVPNTY0SlJKbzVwRDNqektuUEp6RFVwUGliVkFFTDJaRUFUYW9Ra01yZXFfU2F6SjA3anhXdW1uY2JRNlhwd3lEcTNNTXNiTTZqNE90amdTdUVVcXciLCJjcmVhdGVkIjoxNjQ5ODM2Mjk3LCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduVXAiLCJhdHRlbXB0IjowLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiYWxpeWEudGVzdGluZ0BnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoyMDA0MzgyNzk1LCJleHBpcnkiOjE2NDk5MjI2OTcsImV4cCI6MTY0OTkyMjY5N30.wpbrmG9b09kxNyfj1PvJolTQjVaOIssSamtWFisAt7Za_R0F-Pln3_TtIlEOQHCSk9eVjQnvVoBO7PJ7757JNQ; school_preprod_embibe-refresh-token=1aca3e5a-e474-45eb-ada6-e89945acdce4'
    }

    response = requests.request("GET", host+url, headers=headers, data=payload)
    return response

def get_book_list():
    results=[]
    fiber_countries_response=get_goal_exams()
    for locale in [{gv.local_to_check}]:
        
        for meta in fiber_countries_response:            
            id_=meta.get("goal_format_reference")+'/'+meta.get("goal_name")+'/'+meta.get("exam_name")
            subjects=get_all_subjects(id_)
            for sub in subjects:
                response=get_all_books(meta.get("exam_name"),sub[0]).json()
                for item in response.get("_items", []):
                    book_name=item.get("name")
                    path=item.get("path")
                    exam_meta={"format_ref":meta.get("goal_format_reference"),"goal":meta.get("goal_name"),"goal_code":meta.get("goal_code"),
                                    "exam":meta.get("exam_name"),"exam_code":meta.get("exam_code"), "vertical": meta.get("vertical"),
                                    "subject":sub[0],"subject_code":sub[1],"subject_name": sub[2],
                                    "book_name":book_name,"path":path}
                    results.append(exam_meta)
    return results

def get_topic_list():
    topic_results=[]
    for val in get_book_list():
        chap_id = val.get("path")
        chapters = get_next_level(chap_id)
        if chapters.status_code==200:
            flag=True
            for dash in chapters.json():
                if dash not in ["Chapter"]: 
                    flag=False
            if not flag:
                continue
            for chapter in chapters.json().get("Chapter"):
                chapter_name=chapter.get("name")
                id_=chap_id+"/"+chapter_name
                topics=get_next_level(id_)
                if topics.status_code==200:
                    for topic in topics.json().get("Topic",[]):
                        topic_display_name=topic.get("display_name")
                        topic_name=topic.get("name")
                        topic_code=topic.get("code")
                        subject_path=val.get("goal")+"/"+val.get("exam")+"/"+val.get("subject_name")
                        subject_lm=(val.get("goal")+"--"+val.get("exam")+"--"+val.get("subject_name")).lower()
                        subject_kv=val.get("goal_code")+"--"+val.get("exam_code")+"--"+val.get("subject_code")
                        subjectId=val.get("format_ref")+"|"+subject_path+"|"+subject_lm+"|"+subject_kv
                        
                        topic_dict = {"format_ref":val.get("format_ref"),
                                        "goal":val.get("goal"),
                                        "goal_code":val.get("goal_code"),
                                        "exam":val.get("exam"),
                                        "exam_code":val.get("exam_code"),
                                        "vertical": val.get("vertical"),
                                        "subject":val.get("subject"),
                                        "subject_code":val.get("subject_code"),
                                        "book_name":val.get("book_name"),
                                        "path":val.get("path"),
                                        "chapter_name":chapter_name,
                                        "chapter_code": chapter.get("code"),
                                        "topic_name":topic_name,
                                        "topic_display_name": topic_display_name,
                                        "chapter_kve_code": chapter.get("lpcode"),
                                        "topic_code":topic_code,
                                        "subject_id": subjectId,
                        }
                        try:
                            topic_dict["topic_kve_code"] =  topic.get("learning_maps",[])[0].get("learnpath_code")                            
                            topic_dict["topic_learnpath"] =  topic.get("learning_maps",[])[0].get("learnpath_name", "")
                            # topic_dict["topic_kve_code"] =  topic.get("learning_maps",[{}])[0].get("learnpath_code", "")
                        except:
                            topic_dict["topic_kve_code"] = topic.get("lpcode")
                            topic_dict["topic_learnpath"] = topic.get("learnpath_name")

                        print(topic_dict)
                        topic_results.append(topic_dict)
    return topic_results

def get_topics_from_filter(filter_string, topic_list, meta):
    url = f"/cg-fiber-ms/fiber_app/learning_maps/filters/{filter_string}?locale={gv.local_to_check}"
    response = callAPI(url, payload = {}, method = "GET")
    
    if response.status_code == 200:
        data = response.json()
        if "Topic" in data:
            topic_list.extend([{ **meta, "topic_name": x.get("name"), "topic_display_name": x.get("display_name"), "topic_code": x.get("code"), "learnpath_name": x.get("learnpath_name")} for x in data.get("Topic")])
            print("total length: {}".format(len(topic_list)))
            return
        else:
            key = list(data.keys())[0]
            for entry in data.get(key):
                new_filter = filter_string+"/"+entry.get("name")
                _meta = copy.deepcopy(meta)
                if key == "Subject":
                    _meta["subject"] = entry.get("name")
                    _meta["subject_code"] = entry.get("code")
                if key == "Chapter":
                    _meta["chapter_name"] = entry.get("name")
                    _meta["chapter_code"] = entry.get("code")
                get_topics_from_filter(new_filter, topic_list, _meta)

def get_topics_from_goal_meta(goal_exam_meta, topics):
    meta = {
        "format_ref": goal_exam_meta.get("goal_format_reference"),
        "goal": goal_exam_meta.get("goal_name"),
        "vertical": goal_exam_meta.get("vertical"),
        "goal_code": goal_exam_meta.get("goal_code"),
        "exam": goal_exam_meta.get("exam_name"),
        "exam_code": goal_exam_meta.get("exam_code")
    }
    filter_string = "{}/{}/{}".format(goal_exam_meta.get("goal_format_reference"), goal_exam_meta.get("goal_name"), goal_exam_meta.get("exam_name"))
    get_topics_from_filter(filter_string, topics, meta)


def get_big_book_topic_list():
    topics = []
    for goal_exam_meta in get_goal_exams():
        get_topics_from_goal_meta(goal_exam_meta, topics)
    return topics

