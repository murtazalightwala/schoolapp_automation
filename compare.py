from Utils import globalVar as gv
from Utils.callAPI import callAPI
from responsereaders import SearchQuestionsResponseReader, BooksandQuestionsResponseReader




class CompareTopics():
    def __init__(self):
        pass

    def get_results_from_student_app(self, topic):
        
        result = {}

        params = {
            "format_reference": topic.get("format_ref"),
            "learnpath_name": topic.get("learnpath_name"),
            "locale": "en"
        }

        response = callAPI(url = "/cg-fiber-ms/books_and_questions", payload= {}, method = "GET", params = params)
        books_and_questions_response_reader = BooksandQuestionsResponseReader(response)
        result["book_questions in student app"] = books_and_questions_response_reader.get_number_of_questions("book_questions")
        result["non_book_questions in student app"] = books_and_questions_response_reader.get_number_of_questions("non_book_questions")
        result["total_questions in student app"] = books_and_questions_response_reader.get_number_of_questions("total_questions")

        return result
    
    
    
    def get_results_from_search_query(self, topic):
        report = {}

        params = {
            "query": topic.get("topic_name"),
            "facet": "questions",
            "user_goal": topic.get("goal"),
            "user_exam": topic.get("exam"),
            "goal": topic.get("goal"),
            "exam": topic.get("exam"),
            "locale": "en",
            "cross_grade": True
        }
        response = callAPI(url = "/fs_ms/v2/search", payload = {}, method = "GET", params = params)
        search_responsereader = SearchQuestionsResponseReader(response)
        report["search query API response"] = search_responsereader.get_status_code()
        report["No of results in search query"] = search_responsereader.get_number_of_questions()
        return report

    def get_result(self, topic):
        report = {}
        report["topic_meta"] = topic
        report["Goal"] = topic.get("goal")
        report["Exam"] = topic.get("exam")
        report["Book"] = topic.get("book_name", "Embibe Big Book")
        report["Vertical"] = topic.get("vertical")
        report["Chapter"] = topic.get("chapter_name")
        report["Topic"] = topic.get("topic_name")

        search_results = self.get_results_from_search_query(topic)
        student_app_results = self.get_results_from_student_app(topic)
        report = {**report, **search_results, **student_app_results}
        return report
