from queue import Queue
import shutil, os
from Utils import globalVar as gv
from Utils.sheetUtils import update_sheet_by_df
from compare import CompareTopics
from utilities import get_verticalwise_big_book_topics, setup_gv, get_all_queue_result
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
import random, time
from Utils.update_results import ResultsUpdater
import sys




def async_sheet_updater():
    global report
    global kill_updater
    global results_updater
    while True:        
        time.sleep(300)
        _report = get_all_queue_result(report)
        results_updater.update_in_db(_report)
        try:
            df = pd.DataFrame(_report)
        except:
            print(f"Error in updating; report size {len(_report)}!!!")
            continue
        report_dfs = {}
        # for vertical in df["Vertical"].unique().tolist():
        #     report_dfs[vertical] = df[df["Vertical"] == vertical]
        #     report_dfs[vertical].to_csv("Results/" + vertical + "_compare_search_questions_report.csv")
        #     if gv.call_type == "full":
        #         update_sheet_by_df("1fsTyDsCOHehrP2I5U-fqc6gj7FjyY3PV3vu4o0uKRU8", report_dfs[vertical], vertical)
        print(f"Updating {len(_report)} ids at {time.time()}!!!!")
        if kill_updater:
            break
    return

def async_run(topic, report):
    report.put(CompareTopics().get_result(topic))

def main(report_list):
    global kill_updater, results_updater
    try:
        shutil.rmtree("Results")
    except:
        pass
    try:
        os.mkdir("Results")
    except:
        pass
    topic_list = get_verticalwise_big_book_topics(vertical = gv.vertical)
    # topic_list = random.choices(topic_list, k = 10)
    sheet_update_thread = Thread(target = async_sheet_updater)
    with ThreadPoolExecutor(max_workers = 100) as executor:
        sheet_update_thread.start()
        executor.map(async_run, topic_list, [report]*len(topic_list))

    # for topic in topic_list:
    #     async_run(topic, report)

    kill_updater = True
    # report_df = pd.DataFrame(report_list)
    # report_df.to_csv("Results/compare_search_results.csv")
    results_updater.update_in_db(get_all_queue_result(report))
    print("updating report meta")
    results_updater.mark_as_complete()
    print("report meta updated")
    sys.exit()
    # for vertical in report_df["Vertical"].unique().tolist():
    #         if gv.call_type == "full":
    #             update_sheet_by_df("1fsTyDsCOHehrP2I5U-fqc6gj7FjyY3PV3vu4o0uKRU8", report_df[report_df["Vertical"] == vertical], vertical)

if __name__ == "__main__":
    setup_gv()
    report = Queue()
    results_updater = ResultsUpdater("compare_search", gv.local_to_check)
    kill_updater = False
    main(report)