import shutil, os
from Utils import globalVar as gv
from Utils.sheetUtils import update_sheet_by_df
from fs_ms import FS_MS
from utilities import get_verticalwise_big_book_topics, setup_gv, get_class_id, get_verticalwise_book_topics, get_all_queue_result
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
import pandas as pd
import random, time
from queue import Queue
from sheets_list import fs_ms_search_results
from Utils.update_results import ResultsUpdater
from Utils.db import QADb
import sys

def async_sheet_updater():
    global report
    global results_updater
    global kill_updater
    while True:
        time.sleep(300)
        _report = get_all_queue_result(report)
        print(len(_report))
        results_updater.update_in_db(_report)
        try:
            df = pd.DataFrame(_report)

        except:
            print(f"Error in updating; report size {len(_report)}!!!")
            continue
        report_dfs = {}
        # for vertical in df["Vertical"].unique().tolist():
        #     report_dfs[vertical] = df[df["Vertical"] == vertical]
        #     report_dfs[vertical].to_csv("Results/" + vertical + "_search_report.csv")
        #     if gv.call_type == "full":
        #         update_sheet_by_df(fs_ms_search_results.get(gv.local_to_check), report_dfs[vertical], vertical)
        print(f"Updating {len(_report)} ids at {time.time()}!!!!")
        if kill_updater:
            break
    return

def async_run(topic, report):
    report.put(FS_MS(gv.local_to_check, gv.headers, gv.teacher_email, gv.school_preprod_embibe_token).check_search(topic))

def main(report_list):
    qadb = QADb()
    global kill_updater, results_updater
    
    try:
        shutil.rmtree("Results")
    except:
        pass
    try:
        os.mkdir("Results")
    except:
        pass
    topic_list = get_verticalwise_big_book_topics(vertical = gv.vertical)
    topic_list.extend(get_verticalwise_book_topics(vertical = gv.vertical))
    # topic_list = random.choices(topic_list, k = 10)
    sheet_update_thread = Thread(target = async_sheet_updater, name = "Sheet Update Thread")
    with ThreadPoolExecutor(max_workers = 25) as executor:
        sheet_update_thread.start()
        executor.map(async_run, topic_list, [report_list]*len(topic_list))

    # for topic in topic_list:
    #     async_run(topic, report)

    kill_updater = True
    # report_df = pd.DataFrame(report_list)
    # report_df.to_csv("Results/search_results.csv")
    results_updater.update_in_db(get_all_queue_result(report))
    print("updating report meta")
    results_updater.mark_as_complete()
    print("report meta updated")
    sys.exit()
    # for vertical in report_df["Vertical"].unique().tolist():
    #         if gv.call_type == "full":
    #             update_sheet_by_df(fs_ms_search_results.get(gv.local_to_check), report_df[report_df["Vertical"] == vertical], vertical)

if __name__ == "__main__":
    setup_gv()
    report = Queue()
    results_updater = ResultsUpdater("fs_ms", gv.local_to_check)
    kill_updater = False
    main(report)