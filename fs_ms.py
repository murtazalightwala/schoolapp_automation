from Utils import globalVar as gv
from Utils.callAPI import callAPI
from responsereaders import FSMSSearchResponseReader, FSMSAMPSearchResponseReader
from all_urls import fs_ms_ampsearch_url,fs_ms_search_url


class FS_MS:
    
    def __init__(self, locale, headers, teacher_id, school_preprod_embibe_token) -> None:
        self.locale = locale
        self.headers = headers
        self.teacher_id = teacher_id
        self.headers["embibe-token"] = school_preprod_embibe_token
        return
    
    def get_content_from_search(self, topic, content_type):
        params = {
            "facet": content_type,
            "start": 0,
            "size": 100,
            "user_goal": topic.get("goal"),
            "user_exam": topic.get("exam"),
            "locale": self.locale,
            "cross_grade": True
        }
        if topic.get("type") == "book":
            params["filter"] = "book_topic_code.keyword:{}".format(topic.get("topic_code"))
        else:
            params["filter"] = "topic_code.keyword:{}".format(topic.get("topic_code"))

        response = callAPI(fs_ms_search_url, payload={}, method="GET", headers= self.headers, params = params)
        fsms_search_response_reader = FSMSSearchResponseReader(response)
        return {"API status code":fsms_search_response_reader.get_status_code(), "content_count": fsms_search_response_reader.get_content_count()}
    
    def get_content_from_ampsearch(self, topic, content_type):
        filters = f"filter=asset_type%3A{content_type}&filter=published%3Atrue&filter=visibility%3Apublic"
        url = fs_ms_ampsearch_url.format(self.teacher_id, self.locale, topic.get("topic_code"), filters)
        response = callAPI(url, payload = {}, method = "GET", headers = self.headers)
        fsms_amp_search_response_reader = FSMSAMPSearchResponseReader(response)
        return {"API status code": fsms_amp_search_response_reader.get_status_code(), "content_count": fsms_amp_search_response_reader.get_content_count()}

    def check_search(self, topic):
        report = {}
        report["topic_meta"] = topic
        report["topic_id"] = topic["exam_code"] + "--" + topic["subject_code"] + "--" + topic["topic_code"]+ "--"+ self.locale
        report["Goal"] = topic.get("goal")
        report["Exam"] = topic.get("exam")
        report["Book"] = topic.get("book_name", "Embibe Big Book")
        report["Vertical"] = topic.get("vertical")
        report["Chapter"] = topic.get("chapter_name")
        report["Topic"] = topic.get("topic_name")

        search_content_types = [
            "videos",
            "questions"
        ]

        amp_search_content_types = [
            "3d",
            "video",
            "animation",
            "image",
            "illustration",
            "vr_image"
        ]

        for content_type in search_content_types:
            _content_count = self.get_content_from_search(topic, content_type)
            report[f"search API | {content_type} | status code"] = _content_count.get("API status code")
            report[f"search API | {content_type} | count"] = _content_count.get("content_count")
        
        for content_type in amp_search_content_types:
            _content_count = self.get_content_from_ampsearch(topic, content_type)
            report[f"ampsearch API | {content_type} | status code"] = _content_count.get("API status code")
            report[f"ampsearch API | {content_type} | count"] = _content_count.get("content_count")
        
        return report