from Utils.callAPI import callAPI
from copy import deepcopy
from responsereaders import *
import random
import json
from all_urls import assign_homework_urls
from datetime import datetime
from book_ops import BookOps


class HomeworkAssigner:

    def __init__(self, class_name, teacher_id, school_id, locale_to_check, get_class_id, **kwargs): #get_class_id is a function to return value of class_id and takes topic dict object
        self.class_name = class_name
        self.teacher_id = teacher_id
        self.school_id = school_id
        self.locale_to_check = locale_to_check
        self.get_class_id = get_class_id
        if "slot_id" in kwargs:
            self.slot_id = kwargs.get("slot_id")
        else:
            self.slot_id = "6222034009a8894bfaafa810"

    homework_types = [
        {
            "homework_format": "PRE_CLASS_CYO_HOMEWORK", 
            "next_step": "CYO_CONFIRM_SYLLABUS"
        },
        {
            "homework_format": "PRE_CLASS_AJ_HOMEWORK", 
            "next_step": "AJ_CONFIRM_SYLLABUS"
        },
        {
            "homework_format": "PRE_CLASS_AP_HOMEWORK", 
            "next_step": "AP_CONFIRM_SYLLABUS"
        },
        {
            "homework_format": "PRE_CLASS_EP_TEST", 
            "next_step": "EP_SELECT_TEST"
        },
        {
            "homework_format": "POST_CLASS_CYO_HOMEWORK", 
            "next_step": "CYO_CONFIRM_SYLLABUS"
        },
        {
            "homework_format": "POST_CLASS_AJ_HOMEWORK", 
            "next_step": "AJ_CONFIRM_SYLLABUS"
        },
        {
            "homework_format": "POST_CLASS_AP_HOMEWORK", 
            "next_step": "AP_CONFIRM_SYLLABUS"
        },
        {
            "homework_format": "POST_CLASS_EP_TEST", 
            "next_step": "EP_SELECT_TEST"
        }
    ]

    confirm_syllabus_list = [
        {
            "homework_type": "PRE_CLASS_CYO_HOMEWORK",
            "config_step": "CYO_CONFIRM_SYLLABUS",
            "next_step": "CYO_QUESTION_SELECTION"
        },
         {
            "homework_type": "PRE_CLASS_AJ_HOMEWORK",
            "config_step": "AJ_CONFIRM_SYLLABUS",
            "next_step": "AJ_ADD_DETAILS"
        },
         {
            "homework_type": "PRE_CLASS_AP_HOMEWORK",
            "config_step": "AP_CONFIRM_SYLLABUS",
            "next_step": "AP_ADD_DETAILS"
        },
        {
            "homework_type": "POST_CLASS_CYO_HOMEWORK",
            "config_step": "CYO_CONFIRM_SYLLABUS",
            "next_step": "CYO_QUESTION_SELECTION"
        },
         {
            "homework_type": "POST_CLASS_AJ_HOMEWORK",
            "config_step": "AJ_CONFIRM_SYLLABUS",
            "next_step": "AJ_ADD_DETAILS"
        },
         {
            "homework_type": "POST_CLASS_AP_HOMEWORK",
            "config_step": "AP_CONFIRM_SYLLABUS",
            "next_step": "AP_ADD_DETAILS"
        }
    ]

    add_details_list = [
        {
            "homework_type": "PRE_CLASS_CYO_HOMEWORK",
            "config_step": "CYO_ADD_DETAILS"
        },
        {
            "homework_type": "PRE_CLASS_AJ_HOMEWORK",
            "config_step": "AJ_ADD_DETAILS"
        },
        {
            "homework_type": "PRE_CLASS_AP_HOMEWORK",
            "config_step": "AP_ADD_DETAILS"
        },
        {
            "homework_type": "POST_CLASS_CYO_HOMEWORK",
            "config_step": "CYO_ADD_DETAILS"
        },
        {
            "homework_type": "POST_CLASS_AJ_HOMEWORK",
            "config_step": "AJ_ADD_DETAILS"
        },
        {
            "homework_type": "POST_CLASS_AP_HOMEWORK",
            "config_step": "AP_ADD_DETAILS"
        }
    ]

    atgConfig_payload = {
        "name": "",
        "selectedFormatType": "",
        "nextStep": "",
        "slotId": "6222034009a8894bfaafa810",
        "classIds": [
            
        ],
        "teacherId": "",
        "chapterKveCode": "static--static--static--static--static",
        "parentKveCode": None,
        "learningPath": "",
        "learningMaps": [
            {
                "name": "",
                "learningPathFormatName": "goal--exam--subject--unit--chapter--topic",
                "topicKveCode": "static--static--static--static--static--static",
                "meta": {
                    "name": "",
                    "chapter_code": "",
                    "display_name": "",
                    "topic_code": "",
                    "bookType": "Book",
                    "chapter_name": "",
                    "book_path": "",
                    "bookLearningPathFormatName": ""
                }
            }
        ],
        "schoolId": "",
        "subjectId": "",
        "subjectName": "",
        "timetableId": "62419d625fe43223deb8a3dd",
        "templateId": "61c38789acb4f1190e9950a6"
            }
    
    def get_first_step(homework_format):
        for homework_type in __class__.homework_types:
            if homework_type.get("homework_format") == homework_format:
                return homework_type.get("next_step")
        return ""

    def get_confirm_syllabus_meta(homework_format):
        for homework_type in __class__.confirm_syllabus_list:
            if homework_type.get("homework_type") == homework_format:
                return homework_type
        return {}
    
    def get_add_details_meta(homework_format):
        for homework_type in __class__.add_details_list:
            if homework_type.get("homework_type") == homework_format:
                return homework_type.get("config_step")    
        return ""

    def create_atg_config(self, topic, homework_format):
        payload = deepcopy(self.atgConfig_payload)
        payload["name"] = self.class_name
        payload["slotId"] = self.slot_id
        payload["classIds"] = [self.get_class_id(topic)]
        payload["teacherId"] = self.teacher_id
        payload["schoolId"] = self.school_id
        payload["selectedFormatType"] = homework_format
        payload["nextStep"] = __class__.get_first_step(homework_format)
        payload["learningMaps"][0]["name"] = topic.get("topic_name")
        payload["learningMaps"][0]["meta"]["chapter_code"] = topic.get("chapter_code")
        payload["learningMaps"][0]["meta"]["display_name"] = topic.get("topic_display_name")
        payload["learningMaps"][0]["meta"]["topic_code"] = topic.get("topic_code")
        payload["learningMaps"][0]["meta"]["chapter_name"] = topic.get("chapter_name")
        payload["learningMaps"][0]["meta"]["book_path"] = topic.get("path")
        payload["learningMaps"][0]["topicKveCode"] = topic.get("topic_kve_code")
        payload["learningMaps"][0]["meta"]["name"] = topic.get("topic_name")
        payload["chapterKveCode"] = topic.get("chapter_kve_code")
        payload["learningMaps"][0]["meta"]["bookLearningPathFormatName"] = "book code--grade--subject--chapter--topic--sub-topic--sub-sub-topic--sub-sub-sub-topic--sub-sub-sub-sub-topic"
        payload["subjectId"] = topic.get("subject_id")
        payload["learningPath"] = topic.get("topic_learnpath")
        payload["subjectName"] = topic.get("subject")
        print(topic.get("topic_code"), topic.get("topic_name"))
        print(json.dumps(payload))
        response = callAPI(url = assign_homework_urls.get("create").format(self.locale_to_check), payload = payload, method = "POST")
        create_atg_config_response_reader = ATGConfigResponseReader(response)
        return create_atg_config_response_reader
    
    def get_syllabus(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("get_syllabus").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return GetSyllabusResponseReader(response)
    
    def get_chapter_topics(self, atg_config_id, chapter_code):
        response = callAPI(url = assign_homework_urls.get("get_chapter_topics").format(atg_config_id, chapter_code, self.locale_to_check), payload = {}, method = "GET")
        return GetChapterTopicResponseReader(response)
    
    def get_books_content(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("get_books_content").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return GetBooksContentResponseReader(response)
    
    def set_activity_content(self, atg_config_id, book_list):
        payload = {
            "bookList": book_list
        }
        response = callAPI(url = assign_homework_urls.get("set_activity_content").format(atg_config_id, self.locale_to_check), payload = payload, method = "POST")
        return SetActivityContentResponseReader(response)
    
    def confirm_syllabus(self, atg_config_id, homework_type, book_list, learn = True, practice = True):
        payload_meta = __class__.get_confirm_syllabus_meta(homework_type)
        payload = {
                "configStepType": payload_meta.get("config_step"),
                "learn": learn,
                "practice": practice,
                "nextStep": payload_meta.get("next_step"),
                "bookList": book_list
        }
        response = callAPI(url = assign_homework_urls.get("confirm_syllabus").format(atg_config_id, self.locale_to_check), payload = payload, method = "PUT")
        return ConfirmSyllabusResponseReader(response)
    
    def get_homework_chapters(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("get_homework_chapters").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return GetHomeworkChaptersResponseReader(response)
    
    def get_homework_topics(self, atg_config_id, chapter_code):
        response = callAPI(url = assign_homework_urls.get("get_homework_topics").format(atg_config_id, chapter_code, self.locale_to_check), payload = {}, method = "GET")
        return GetHomeworkTopicsResponseReader(response)
    
    def get_topic_videos(self, atg_config_id, topic_code):
        response = callAPI(url = assign_homework_urls.get("get_topic_videos").format(atg_config_id, topic_code, self.locale_to_check), payload = {}, method = "GET")
        return GetTopicVideosResponseReader(response)

    def get_topic_questions(self, atg_config_id, topic_code):
        response = callAPI(url = assign_homework_urls.get("get_topic_questions").format(atg_config_id, topic_code, self.locale_to_check), payload = {}, method = "POST")
        return GetTopicQuestionsResponseReader(response)
    
    def set_videos(self, atg_config_id, homework_type, topic_code, video_id_list):
        payload = {
            "topicVideoMap":{
                topic_code: video_id_list
            },
            "configStepType": "CYO_VIDEO_SELECTION",
            "nextStep": "CYO_QUESTION_SELECTION"

        }
        response = callAPI(url = assign_homework_urls.get("set_videos").format(atg_config_id, self.locale_to_check), payload = payload, method = "PUT")
        if response.status_code != 200:
            print(atg_config_id)
            print(payload)
        return SetVideosResponseReader(response)


    def set_questions(self, atg_config_id, homework_type, topic_code, question_code_list):
        payload = {
            "topicQuestionMap":{
                topic_code: question_code_list
            },
            "configStepType": "CYO_QUESTION_SELECTION",
            "nextStep": "CYO_ADD_DETAILS"

        }
        response = callAPI(url = assign_homework_urls.get("set_questions").format(atg_config_id, self.locale_to_check), payload = payload, method = "PUT")
        return SetQuestionsResponseReader(response)
    
    def get_all_tests(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("get_all_tests").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return GetAllTestsResponseReader(response)
    
    def get_all_chapters(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("get_all_chapters").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return ChaptersResponseReader(response)
    
    def select_test(self, atg_config_id, test):
        payload = {
            "classReadinessTopics": [],
            "configStepType": "EP_SELECT_TEST",
            "nextStep": "EP_REVIEW_QUESTIONS",
            "test": test
        }
        response = callAPI(url = assign_homework_urls.get("select_test").format(atg_config_id, self.locale_to_check), payload= payload, method = "PUT")
        return SelectTestResponseReader(response)

    def get_all_test_questions(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("get_all_test_questions").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return TestQuestionsResponseReader(response)
    
    def review_questions(self, atg_config_id):
        payload = {
            "configStepType": "EP_REVIEW_QUESTIONS",
            "nextStep": "EP_TEST_SCHEDUlE"
        }
        response = callAPI(url = assign_homework_urls.get("review_questions").format(atg_config_id, self.locale_to_check), payload = payload, method = "PUT")
        return ReviewQuestionsResponseReader(response)

    def test_schedule(self, atg_config_id, topic):
        payload = {
            "classIds": [
                    self.get_class_id(topic)
  ],
            "startsAt": int(datetime.now().timestamp()),
            "instructions": "<p>Please complete your Homework</p>",
            "configStepType": "EP_TEST_SCHEDUlE",
            "nextStep": "FINAL_PREVIEW",
            "isScheduled": False
        }

        response = callAPI(url = assign_homework_urls.get("test_schedule").format(atg_config_id, self.locale_to_check), payload = payload, method = "PUT")
        return ATGConfigSetMetaResponseReader(response)

    def set_atg_meta(self, atg_config_id, homework_type, topic):
        payload = {
            "name": str(atg_config_id),
            "classIds": [
                    self.get_class_id(topic)
  ],
            "dueDate": int(datetime.now().timestamp()) + 300000,
            "instructions": "<p>Please complete your Homework</p>",
            "configStepType": __class__.get_add_details_meta(homework_type),
            "nextStep": "FINAL_PREVIEW"

        }

        response = callAPI(url = assign_homework_urls.get("set_atg_meta").format(atg_config_id, self.locale_to_check), payload = payload, method = "PUT")
        return ATGConfigSetMetaResponseReader(response)

    def publish_atg_config(self, atg_config_id):
        response = callAPI(url = assign_homework_urls.get("publish").format(atg_config_id, self.locale_to_check), payload = {}, method = "GET")
        return ATGConfigPublishResponseReader(response)
    
    def get_book_kv_path(self, topic):
        response = callAPI(url = assign_homework_urls.get("fiber_ms_book").format(topic.get("path"), self.locale_to_check, topic.get("exam_code"), topic.get("goal_code")), payload = {}, method = "GET")
        fiber_ms_book_response_reader = FiberMSBookResponseReader(response)
        return fiber_ms_book_response_reader.get_book_kv_path()
    
    def get_count_from_student_app(self, topic):
        kv_path = self.get_book_kv_path(topic) + "/" + topic.get("chapter_code")
        response = callAPI(url = assign_homework_urls.get("fiber_app_lmf").format(kv_path, self.locale_to_check), payload = {}, method = "GET")
        fiber_app_lmf_response_reader = FiberAppLMFResponseReader(response)
        try:
            videos_count = fiber_app_lmf_response_reader.get_video_count(topic)
            question_count = fiber_app_lmf_response_reader.get_question_count(topic)
        except:
            print("Error fetching from student app for ", topic.get("topic_code"))
            videos_count = -1
            question_count = -1
        return videos_count, question_count

    def assign_cyoh(self, topic, format_type = "POST_CLASS_CYO_HOMEWORK"):
        book_ops = BookOps()
        all_books = book_ops.list_all_books_on_school_app(topic.get("exam"), topic.get("subject"))
        for book in all_books:
            try:
                book_ops.add_book(book.get("id"), topic, book.get("type"), self.get_class_id(topic))
            except:
                print("Book Not Added: ", book.get("id"))
        cyoh_report = {}
        cyoh_create_response_reader = self.create_atg_config(topic, format_type)
        atg_config_id = cyoh_create_response_reader.get_atg_config_id()
        cyoh_report["CYOH | ATG Config ID"] = atg_config_id
        cyoh_report["CYOH | create response status code"] = cyoh_create_response_reader.get_status_code()
        # cyoh_report["CYOH | create error message"] = cyoh_create_response_reader.get_error_message()
        if atg_config_id == "Not created":
            return cyoh_report
        get_syllabus_response_reader = self.get_syllabus(atg_config_id)
        # cyoh_report["CYOH | syllabus API status code"] = get_syllabus_response_reader.get_status_code()
        # cyoh_report["CYOH | syllabus API error message"] = get_syllabus_response_reader.get_error_message()
        chapter_topics_response_reader = self.get_chapter_topics(atg_config_id, topic.get("chapter_code"))
        # cyoh_report["CYOH | chapter topics API status code"] = chapter_topics_response_reader.get_status_code()
        # cyoh_report["CYOH | chapter topics API error message"] = chapter_topics_response_reader.get_error_message()
        books_content_reponse_reader = self.get_books_content(atg_config_id)
        cyoh_report["CYOH | books content API status code"] = books_content_reponse_reader.get_status_code()
        # cyoh_report["CYOH | books content API error message"] = books_content_reponse_reader.get_error_message()
        active_book = books_content_reponse_reader.get_active_book(topic)
        print("Active Book: ", active_book)
        activity_content_response_reader = self.set_activity_content(atg_config_id, [active_book])
        cyoh_report["CYOH | Selected Book"] = active_book.get("bookPath")
        cyoh_report["CYOH | set activity content API status code"] = activity_content_response_reader.get_status_code()
        # cyoh_report["CYOH | set activity content API error message"] = activity_content_response_reader.get_error_message()
        cyoh_report["CYOH | set activity content question count"] = activity_content_response_reader.get_question_count()
        confirm_syllabus_response_reader = self.confirm_syllabus(atg_config_id, format_type, [active_book], True, True)
        cyoh_report["CYOH | confirm syllabus API status code"] = confirm_syllabus_response_reader.get_status_code()
        cyoh_report["CYOH | confirm syllabus API error message"] = confirm_syllabus_response_reader.get_error_message()
        homework_chapters_response_reader = self.get_homework_chapters(atg_config_id)
        # cyoh_report["CYOH | homework chapters API status code"] = homework_chapters_response_reader.get_status_code()
        # cyoh_report["CYOH | homework chapters API error message"] = homework_chapters_response_reader.get_error_message()
        homework_topics_response_reader = self.get_homework_topics(atg_config_id, topic.get("chapter_code"))
        # cyoh_report["CYOH | homework topics API status code"] = homework_topics_response_reader.get_status_code()
        # cyoh_report["CYOH | homework topics API error message"] = homework_topics_response_reader.get_error_message()
        student_app_video_count, student_app_question_count = self.get_count_from_student_app(topic)
        cyoh_report["CYOH | video count in student app"] = student_app_video_count
        homework_videos_response_reader = self.get_topic_videos(atg_config_id, topic.get("topic_code"))
        cyoh_report["CYOH | topic videos API status code"] = homework_videos_response_reader.get_status_code()
        # cyoh_report["CYOH | topic videos API status error message"] = homework_videos_response_reader.get_error_message()
        cyoh_report["CYOH | topic videos API video count"] = homework_videos_response_reader.get_video_count()
        video_id_list = homework_videos_response_reader.get_video_codes()
        set_videos_response_reader = self.set_videos(atg_config_id, format_type, topic.get("topic_code"), video_id_list)
        cyoh_report["CYOH | set videos API status code"] = set_videos_response_reader.get_status_code()
        # cyoh_report["CYOH | set videos API error message"] = set_videos_response_reader.get_error_message()
        homework_questions_response_reader = self.get_topic_questions(atg_config_id, topic.get("topic_code"))
        cyoh_report["CYOH | question count in student app"] = student_app_question_count
        cyoh_report["CYOH | topic questions API status code"] = homework_questions_response_reader.get_status_code()
        # cyoh_report["CYOH | topic questions API error message"] = homework_questions_response_reader.get_error_message()
        cyoh_report["CYOH | topic questions API question count"] = homework_questions_response_reader.get_question_count()
        question_code_list = homework_questions_response_reader.get_question_codes()
        set_questions_response_reader = self.set_questions(atg_config_id, format_type, topic.get("topic_code"), question_code_list)
        cyoh_report["CYOH | set questions API status code"] = set_questions_response_reader.get_status_code()
        # cyoh_report["CYOH | set questions API error message"] = set_questions_response_reader.get_error_message()
        set_atg_meta_response_reader = self.set_atg_meta(atg_config_id, format_type, topic)
        cyoh_report["CYOH | set meta API status code"] = set_atg_meta_response_reader.get_status_code()
        # cyoh_report["CYOH | set meta API error message"] = set_atg_meta_response_reader.get_error_message()
        publish_atg_config_response_reader = self.publish_atg_config(atg_config_id)
        cyoh_report["CYOH | publish atg API status code"] = publish_atg_config_response_reader.get_status_code()
        # cyoh_report["CYOH | publish atg API error message"] = publish_atg_config_response_reader.get_error_message()
        cyoh_report["CYOH | LearningBundleId"] = publish_atg_config_response_reader.get_learning_bundle_id()
        return cyoh_report

    def assign_achieve_journey(self, topic):
        achieve_journey_report = {}
        pass

    def assign_adaptive_practice(self, topic, format_type = "POST_CLASS_AP_HOMEWORK"):
        adaptive_practice_report = {}
        ap_create_response_reader = self.create_atg_config(topic, format_type)
        atg_config_id = ap_create_response_reader.get_atg_config_id()
        adaptive_practice_report["AP | ATG Config ID"] = atg_config_id
        adaptive_practice_report["AP | create response status code"] = ap_create_response_reader.get_status_code()
        # adaptive_practice_report["AP | create error message"] = ap_create_response_reader.get_error_message()
        if atg_config_id == "Not created":
            return adaptive_practice_report
        get_syllabus_response_reader = self.get_syllabus(atg_config_id)
        adaptive_practice_report["AP | syllabus API status code"] = get_syllabus_response_reader.get_status_code()
        # adaptive_practice_report["AP | syllabus API error message"] = get_syllabus_response_reader.get_error_message()
        chapter_topics_response_reader = self.get_chapter_topics(atg_config_id, topic.get("chapter_code"))
        adaptive_practice_report["AP | chapter topics API status code"] = chapter_topics_response_reader.get_status_code()
        # adaptive_practice_report["AP | chapter topics API error message"] = chapter_topics_response_reader.get_error_message()
        books_content_reponse_reader = self.get_books_content(atg_config_id)
        adaptive_practice_report["AP | books content API status code"] = books_content_reponse_reader.get_status_code()
        # adaptive_practice_report["AP | books content API error message"] = books_content_reponse_reader.get_error_message()
        active_book = books_content_reponse_reader.get_active_book(topic)
        confirm_syllabus_response_reader = self.confirm_syllabus(atg_config_id, format_type, [active_book], False, True)
        adaptive_practice_report["AP | confirm syllabus API status code"] = confirm_syllabus_response_reader.get_status_code()
        # adaptive_practice_report["AP | confirm syllabus API error message"] = confirm_syllabus_response_reader.get_error_message()
        set_atg_meta_response_reader = self.set_atg_meta(atg_config_id, format_type, topic)
        adaptive_practice_report["AP | set meta API status code"] = set_atg_meta_response_reader.get_status_code()
        # adaptive_practice_report["AP | set meta API error message"] = set_atg_meta_response_reader.get_error_message()
        publish_atg_config_response_reader = self.publish_atg_config(atg_config_id)
        adaptive_practice_report["AP | publish atg API status code"] = publish_atg_config_response_reader.get_status_code()
        # adaptive_practice_report["AP | publish atg API error message"] = publish_atg_config_response_reader.get_error_message()
        adaptive_practice_report["AP | LearningBundleId"] = publish_atg_config_response_reader.get_learning_bundle_id()
        return adaptive_practice_report

    def assign_embibe_preset(self, topic, format_type = "POST_CLASS_EP_TEST"):
        embibe_preset_report = {}
        ep_create_response_reader = self.create_atg_config(topic, format_type)
        atg_config_id = ep_create_response_reader.get_atg_config_id()
        embibe_preset_report["EP | ATG Config ID"] = atg_config_id
        embibe_preset_report["EP | create response status code"] = ep_create_response_reader.get_status_code()
        # embibe_preset_report["EP | create error message"] = ep_create_response_reader.get_error_message()
        if atg_config_id == "Not created":
            return embibe_preset_report
        chapters_response_reader = self.get_all_chapters(atg_config_id)
        embibe_preset_report["EP | Chapters API status code"] = chapters_response_reader.get_status_code()
        # embibe_preset_report["EP | Chapters API error message"] = chapters_response_reader.get_error_message()
        tests_response_reader = self.get_all_tests(atg_config_id)
        embibe_preset_report["EP | Tests API status code"] = tests_response_reader.get_status_code()
        # embibe_preset_report["EP | Tests API error message"] = tests_response_reader.get_error_message()
        embibe_preset_report["EP | Tests count"] = tests_response_reader.get_test_count()
        test = tests_response_reader.get_test()
        select_test_response_reader = self.select_test(atg_config_id, test)
        embibe_preset_report["EP | select test status code"] = select_test_response_reader.get_status_code()
        # embibe_preset_report["EP | select test error message"] = select_test_response_reader.get_error_message()
        get_all_questions_response_reader = self.get_all_test_questions(atg_config_id)
        embibe_preset_report["EP | all questions API status code"] = get_all_questions_response_reader.get_status_code()
        # embibe_preset_report["EP | all questions API error message"] = get_all_questions_response_reader.get_error_message()
        schedule_test_response_reader = self.test_schedule(atg_config_id, topic)
        embibe_preset_report["EP | schedule test API status code"] = schedule_test_response_reader.get_status_code()
        # embibe_preset_report["EP | schedule test API error message"] = schedule_test_response_reader.get_error_message()
        set_atg_meta_response_reader = self.set_atg_meta(atg_config_id,format_type, topic)
        embibe_preset_report["EP | set meta API status code"] = set_atg_meta_response_reader.get_status_code()
        # embibe_preset_report["EP | set meta API error message"] = set_atg_meta_response_reader.get_error_message()
        publish_atg_config_response_reader = self.publish_atg_config(atg_config_id)
        embibe_preset_report["EP | publish atg API status code"] = publish_atg_config_response_reader.get_status_code()
        # embibe_preset_report["EP | publish atg API error message"] = publish_atg_config_response_reader.get_error_message()
        embibe_preset_report["EP | LearningBundleId"] = publish_atg_config_response_reader.get_learning_bundle_id()
        return embibe_preset_report

        

    def assign_all_homeworks(self, topic):
        report = {}
        report["topic_meta"] = topic
        report["topic_id"] = topic["exam_code"] + "--" + topic["subject_code"] + "--" + topic["topic_code"]+ "--"+ self.locale_to_check
        report["Goal"] = topic.get("goal")
        report["Exam"] = topic.get("exam")
        report["Book"] = topic.get("book_name")
        report["Vertical"] = topic.get("vertical")
        report["Chapter"] = topic.get("chapter_name")
        report["Topic"] = topic.get("topic_name")
        report["Topic Code"] = topic.get("topic_code")

        cyoh_report = self.assign_cyoh(topic)
        ap_report = self.assign_adaptive_practice(topic)
        ep_report = self.assign_embibe_preset(topic)
        report = {**report, **cyoh_report, **ap_report, **ep_report}
        return report
    

    def assign_pre_and_post_homeworks(self, topic):
        report = {}
        report["topic_meta"] = topic
        report["topic_id"] = topic["exam_code"] + "--" + topic["subject_code"] + "--" + topic["topic_code"]+ "--"+ self.locale_to_check
        report["Goal"] = topic.get("goal")
        report["Exam"] = topic.get("exam")
        report["Book"] = topic.get("book_name")
        report["Vertical"] = topic.get("vertical")
        report["Chapter"] = topic.get("chapter_name")
        report["Topic"] = topic.get("topic_name")
        report["Topic Code"] = topic.get("topic_code")

        pre_cyoh_report = self.assign_cyoh(topic, format_type = "PRE_CLASS_CYO_HOMEWORK")
        pre_ap_report = self.assign_adaptive_practice(topic, format_type = "PRE_CLASS_AP_HOMEWORK")
        pre_ep_report = self.assign_embibe_preset(topic, format_type = "PRE_CLASS_EP_TEST")
        post_cyoh_report = self.assign_cyoh(topic, format_type = "POST_CLASS_CYO_HOMEWORK")
        post_ap_report = self.assign_adaptive_practice(topic, format_type = "POST_CLASS_AP_HOMEWORK")
        post_ep_report = self.assign_embibe_preset(topic, format_type = "POST_CLASS_EP_TEST")
        report = {**report, "pre":{**pre_cyoh_report, **pre_ap_report, **pre_ep_report}, "post":{**post_cyoh_report, **post_ap_report, **post_ep_report}}
        return report