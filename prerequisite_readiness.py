from Utils import globalVar as gv
from Utils.callAPI import callAPI
from responsereaders import PreRequisiteResponseReader, StudentPrerequisiteResponseReader, StudentTopicsVideosResponseReader, PrebuiltLessonsResponseReader
from utilities import get_class_id
from all_urls import prerequisite_readiness_url, student_prerequisite_url, student_topic_videos_url, prebuilt_lessons_url






class PrerequisiteReadiness():

    def __init__(self) -> None:
        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cache-Control': 'no-cache',
  'Connection': 'keep-alive',
  'Content-Type': 'application/json',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Pragma': 'no-cache',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
  'embibe-token': "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlFBIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMTMgMDc6MjM6MDQgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTg5OTg5OTkwMCIsInJvb3RPcmdJZCI6IjYzN2M3NjJkNjQxNzAwM2JiMDdjNGFiYiIsImRldmljZUlkIjoiMC4zNzIyMTA2ODQ1MzIwNzk2IiwiZmlyc3ROYW1lIjoiQXV0b21hdGlvbiIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjMsInBhcmVudE9yZ0lkIjoiNjM3Yzc4NmVlZDljNjgwYmEyYThlZGVkIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjM3Y2I2Y2QwOWNjYjg0ZWZmYWQ2NjNlIiwiaWQiOjE1MDQ4MDkxNDQsImVtYWlsIjoicWEuYXV0b21hdGlvbkBlbWJpYmUuY29tIn0.GWLCe6H_z6wzXnTxGFn8CkLLn9by-BckIweLa_Ln3JlvY4Eaqc1NicpdTR-zx2wu8wI1qhyaFbxlywtWGmmB9w",
  'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}
        return

    def get_prerequisite_readiness(self, topic):
        report = {}
        report["topic_meta"] = topic
        report["topic_id"] = topic["exam_code"] + "--" + topic["subject_code"] + "--" + topic["topic_code"]+ "--"+ gv.local_to_check
        report["Goal"] = topic.get("goal")
        report["Exam"] = topic.get("exam")
        report["Book"] = topic.get("book_name", "Embibe Big Book")
        report["Vertical"] = topic.get("vertical")
        report["Chapter"] = topic.get("chapter_name")
        report["Topic"] = topic.get("topic_name")

        payload = {
            "classId": get_class_id(topic),
            "subjectId": "{}--{}--{}".format(topic.get("goal_code"), topic.get("exam_code"), topic.get("subject_code")),
            "kvCode": [topic.get("topic_code")],
            "isLiveClass": False
        }
        response = callAPI(url = prerequisite_readiness_url.format(gv.local_to_check), payload = payload, method = "POST", headers = self.headers)
        prerequisite_response_reader = PreRequisiteResponseReader(response)
        topic_learnpath = topic.get("learnpath_name")
        
        learnmap_id = topic.get("format_ref")+"/"+"/".join(topic_learnpath.split("--"))
        payload = {
    "learnmap_id": learnmap_id,
    "subject": topic.get("subject"),
    "locale": gv.local_to_check
}
        print(payload)
        response = callAPI(url = student_prerequisite_url, payload = payload, method = "POST", headers = {**gv.headers, **{"embibe-token": gv.embibe_token}})
        student_prerequisite_response_reader = StudentPrerequisiteResponseReader(response)
        all_topics = student_prerequisite_response_reader.get_all_topics()
        video_count = 0
        for topic in all_topics:
            response = callAPI(url = student_topic_videos_url.format(topic.get("learnmap_id"), gv.local_to_check),payload = {}, method = "GET", headers = {**gv.headers, **{"embibe-token": gv.embibe_token}})
            print(response.request.url)
            student_topic_video_response_reader = StudentTopicsVideosResponseReader(response)
            video_count+=student_topic_video_response_reader.get_video_count()
            print(topic.get("title"),student_topic_video_response_reader.get_video_count())
        report["API status code"] = prerequisite_response_reader.get_status_code()
        report["Total Videos"] = prerequisite_response_reader.get_total_videos()
        report["Topics"] = prerequisite_response_reader.get_prerequisite_topics()
        report["Student PR | API status code"] = student_prerequisite_response_reader.get_status_code()
        report["Student PR | Total Videos"] = video_count
        report["Student PR | Topics"] = student_prerequisite_response_reader.get_all_topic_codes()
        report["Is Okay"] = report["Total Videos"] == report["Student PR | Total Videos"]
        return report
    
    def get_prebuilt_lessons(self, topic):
        report = {}
        params = {
            "_start": 0,
            "_limit": 2000,
            "duplicate": False,
            "school_id": gv.school_id,
            "subject": topic.get("subject"),
            "topic": topic.get("topic_name"),
            "locale": gv.local_to_check
        }

        response = callAPI(prebuilt_lessons_url, payload = {}, method = "GET", headers = gv.headers, params = params)
        prebuilt_lessons_response_reader = PrebuiltLessonsResponseReader(response)
        report["Prebuilt Lessons API status code"] = prebuilt_lessons_response_reader.get_status_code()
        report["Prebuilt Lessons count"] = prebuilt_lessons_response_reader.get_prebuilt_lessons_count()
        return report

    def run_prerequisite_readiness(self, topic):
        pr_report = self.get_prerequisite_readiness(topic)
        pl_report = self.get_prebuilt_lessons(topic)
        return {**pr_report, **pl_report}