import json
import time

import requests
import Utils.globalVar as gv


class RequestResponse:
    def __init__(self):
        self.status_code = 504
        self.text = "Error due to timeout"

    def json(self):
        return {}


def callAPI(url, payload, method, no_of_iterations=2, **kwargs):
    response = None
    host = gv.host
    if "headers" in kwargs:
        headers = kwargs.get("headers")
    else:
        headers = gv.headers
    if gv.host == 'https://staging1ms.embibe.com' and 'fiber-countries-goals-exams' in url:
        host = 'https://beta-api.embibe.com'

    for i in range(no_of_iterations):
        try:
            response = requests.request(method, host + url, headers=headers, data=json.dumps(payload),
                                        timeout=200, params= kwargs.get("params", {}))
        except Exception as e:
            return RequestResponse()

        if response.status_code == 200:
            return response
        elif response.status_code in [403, 401]:
            gv.initUserMeta()

        time.sleep(1)
    return response
