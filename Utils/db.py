import psycopg2
import psycopg2.extras
import pandas as pd
from pymongo import MongoClient

class QADb:
    def __init__(self):
        self.client = MongoClient("mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@ vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin")
        self.database = self.client["qa-mongodb-preprod"]
        self.topic_list = self.database["topic_lists"]
        self.report_progress_meta = self.database["report_progress_meta"]




def get_cms_connection():
    host = "tech-contentms-preprd.postgres.database.azure.com"
    username = "Embibe007@tech-contentms-preprd"
    password = "W3R5exRO/qG4[pu"
    database = "content_db_rearch_temp"

    con = psycopg2.connect(database=database, user=username, password=password, host=host, port="5432")
    return con


def get_bundle_code_data_from_cms(test_ids, locale):
    print("Getting CMS DB data for locale: ", locale)
    test_ids.replace(", nan", "")
    test_ids = test_ids.replace(" nan,", "")
    test_ids = test_ids.replace("nan", "")
    test_ids = test_ids.replace(", )", ")")
    credentials = get_cms_connection()
    # read in your SQL query results using pandas
    dataframe = pd.read_sql(f"""
				select id,name, path, "version",cg_test_id,code,"language", question_count from mocktest_bundles where 
				(cg_test_id , "version") 
				in (select cg_test_id , max("version") from mocktest_bundles where cg_test_id in {test_ids} and 
				language = '{locale}' group by cg_test_id ) and language = '{locale}';
				""", con=credentials)

    print("Done\n")

    return dataframe


def get_cms_tagged_test(chapter_id):
    credentials = get_cms_connection()
    # read in your SQL query results using pandas
    dataframe = pd.read_sql(f"""
				SELECT t.code
				FROM public.mocktest_bundles t where learning_maps->>0 = '{chapter_id}' 
				or learning_maps->>1 = '{chapter_id}' or learning_maps->>2 = '{chapter_id}' 
				or learning_maps->>3 = '{chapter_id}' or learning_maps->>4 = '{chapter_id}' 
				or learning_maps->>5 = '{chapter_id}' or learning_maps->>6 = '{chapter_id}';
				""", con=credentials)

    return dataframe


def get_test_meta(path):
    credentials = get_cms_connection()
    # read in your SQL query results using pandas
    dataframe = pd.read_sql(f"""
				select id,name, path, "version",code,old_xpath,cg_test_id from mocktest_bundles where path = '{path}';
				""", con=credentials)

    if len(dataframe) == 0:
        return "", "", ""
    return dataframe['id'][0], dataframe['name'][0], dataframe['cg_test_id'][0]


def get_cms_data_by_bundle_id(bundle_ids):
    bundle_ids = bundle_ids.replace(", nan", "")
    bundle_ids = bundle_ids.replace(" nan,", "")
    bundle_ids = bundle_ids.replace("nan", "")
    bundle_ids = bundle_ids.replace(", )", ")")
    credentials = get_cms_connection()
    # read in your SQL query results using pandas
    dataframe = pd.read_sql(f"""
        select id,name, path, "version",cg_test_id,code,"language", question_count from mocktest_bundles 
        where (code , "version") 
        in (select code , max("version") from mocktest_bundles where code in {bundle_ids} group by code );
        """, con=credentials)

    return dataframe


def connect_to_cg():
    client = MongoClient("mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.78/contentgrail")
    db = client["contentgrail"]
    return db
