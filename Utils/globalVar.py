import sys

from Utils.get_system_arguments import get_system_arguments
from Utils.signIn import SignIn_Signup


this = sys.modules[__name__]
this.host = None
this.exam_to_check = None
this.goal_to_check = None
this.local_to_check = None
this.locales = ["hi", "mr", "te", "kn", "ta", "ml", "pa", "as", "gu", "bn", "or", "en"]
this.userId = None
this.embibe_token = None
this.headers = None
this.exam_to_check = None
this.vertical = None
this.goal_to_check = None
this.email = None
this.check_only_books = False
this.skip = 0


def processSysArgs():
    sys_kwargs, args = get_system_arguments()
    this.host = sys_kwargs.get("host", "https://preprodms.embibe.com")
    this.vertical = sys_kwargs.get("vertical")
    this.exam_to_check = sys_kwargs.get("exam")
    this.goal_to_check = sys_kwargs.get("goal")
    this.local_to_check = sys_kwargs.get("locale", "en")
    this.call_type = "ad_hoc"
    this.skip = int(sys_kwargs.get("skip", 0))
    if "full" in args:
        this.call_type = "full"

def initGlobalVars():
    processSysArgs()


    if this.local_to_check is not None:
        this.locales = this.local_to_check.split(",")
    else:
        this.local_to_check = "en"

    if this.host == "https://fiberdemoms.embibe.com":
        this.locales = ["hi"]
        
    this.headers = {
        'Connection': 'keep-alive',
        'Accept': 'application/json',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'Content-Type': 'application/json;charset=UTF-8',
        'browser-id': '1621792748042'
    }


def initUserMeta():
    signInSrc = SignIn_Signup()
    teacher_sign_in = SignIn_Signup()

    # this.email, password = create_new_user(this.host)
    # this.email, password = "test-fiber_checklist@embibe.com", "embibe1234"

    if this.host in ["https://beta-api.embibe.com", "https://api-prod-cf.embibe.com"]:
        this.email = 'ajay.ranwa@embibe.com'
        password = 'embibe1234'
    elif 'fiberdemoms' in this.host:
        this.email = "ajay.ranwa@embibe.com"
        password = "Embibe@123"
    else:
        this.email = 'rinkoomani1@embibe.com'
        password = 'Demo@1234'
    
    if this.host in ["https://beta-api.embibe.com", "https://api-prod-cf.embibe.com"]:
        this.teacher_email = 'ajay.ranwa@embibe.com'
        teacher_password = 'embibe1234'
    elif 'fiberdemoms' in this.host:
        this.teacher_email = "ajay.ranwa@embibe.com"
        teacher_password = "Embibe@123"
    else:
        this.teacher_email = 'qa.automation@embibe.com'
        teacher_password = 'Embibe@1234'
    

    signInSrc.signIn(this.host, this.email, password)
    this.userId = signInSrc.userId
    teacher_sign_in.teacher_signIn(this.host, this.teacher_email, teacher_password)
    this.embibe_token = signInSrc.embibe_token
    # this.headers['embibe-token'] = this.embibe_token
    this.reseller_jwt_token = teacher_sign_in.reseller_jwt_token
    this.teacher_id = teacher_sign_in.teacher_id
    this.school_id = teacher_sign_in.school_id
    this.org_id = teacher_sign_in.org_id
    this.org_token = teacher_sign_in.org_token
    this.class_id = teacher_sign_in.class_id
    this.class_id_list = teacher_sign_in.class_id_list
    this.class_name = teacher_sign_in.class_name
    this.headers['reseller-jwt-token'] = this.reseller_jwt_token
    this.timetable_id = "62419d625fe43223deb8a3dd"
    this.template_id = "61c38789acb4f1190e9950a6"
    this.cookies = teacher_sign_in.cookies
    this.school_preprod_embibe_token = teacher_sign_in.school_preprod_embibe_token


