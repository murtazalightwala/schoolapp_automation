from pymongo import MongoClient
from datetime import datetime
from Utils.db import QADb

class Results:
    client = MongoClient("mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@ vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin")
    database = client["qa-mongodb-preprod"]
    collection = database["results_school_app"]
    report_meta = database["report_progress_meta"]

class ResultsUpdater(Results):

    def __init__(self, report_type, locale):
        self.report_type = report_type
        self.start_time = int(datetime.now().timestamp())
        self.report_id = report_type + "--" + str(self.start_time)
        self.db = QADb()
        print("Locale is: ", locale)
        print("Report ID: ", self.report_id)
        self.locale = locale
        self.db.report_progress_meta.insert_one({"_id": self.report_id, "status": "Running", "start_time": self.start_time, "type": report_type, "locale": locale})
    
    def update_in_db(self, report_list):
        data = [{"_id": x.get("topic_id") + "--" + self.report_id, "report_type": self.report_type, "start_time": self.start_time, "locale": self.locale, "report_id":self.report_id , **x} for x in report_list]
        fail_cases = []
        try:
            self.collection.insert_many(data, ordered = False)
        except Exception as e:
            try:
                fail_cases.extend([x.get("keyValue") for x in e.details.get("writeErrors")])
            except:
                print(str(e))
        return fail_cases

    def mark_as_complete(self):
        self.db.report_progress_meta.find_one_and_update({"_id": self.report_id}, {"$set":{"status": "Completed", "is_latest": True}})
        self.db.report_progress_meta.update_many({"_id":{"$ne": self.report_id}, "locale": self.locale, "type": self.report_type}, {"$set":{"is_latest": False}})
        print("marked complete!!!")

