import time
import datetime
import requests
import json
import string
import random


def generate_random_string(length):
    return ''.join(random.sample(string.ascii_letters + string.digits, length))


def generate_random_str_num(length):
    return ''.join(random.sample(string.digits, length % 10)) + ''.join(random.sample(string.digits, 10))


class SignIn_Signup():
    def __init__(self):
        self.userId = None
        self.password = None
        self.embibe_token = None
        self.email_id = None

    def signIn(self, host, email, password, linked_profile_id=None):

        for i in range(2):
            headers = {
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                'Origin': 'https://staging-fiber-web.embibe.com',
                'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            }

            if "fiberdemoms" in host:
                headers["Origin"] = "https://fiber-demo-web.embibe.com"
            elif "beta" in host or 'api-prod' in host:
                headers["Origin"] = "https://beta-api.embibe.com"

            mobile = 0
            try:
                mobile = int(email)
            except Exception:
                pass

            if mobile == 0:
                payload = {"email": email, "password": password, "device_id": generate_random_str_num(18),
                           "connected_response": True}
            else:
                payload = {"mobile": str(mobile), "password": password, "device_id": generate_random_str_num(18),
                           "connected_response": True}

            url = f"{host}/user_auth_ms/sign-in"

            api_request_time = datetime.datetime.now().timestamp()
            response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
            api_response_time = datetime.datetime.now().timestamp()

            if response.status_code == 200:
                _embibe_token = response.headers["embibe-token"]
                headers['embibe-token'] = _embibe_token
                print(_embibe_token)

                conn_prof_url = f"{host}/user_auth_ms/connected_profiles"

                response = requests.request("GET", conn_prof_url, headers=headers, data={})

                if response.status_code == 200:
                    if "linked_profiles" in response.json().get("data", {}) and len(
                            response.json().get("data", {}).get("linked_profiles", [])) > 0:
                        linked_profiles = response.json().get("data", {}).get("linked_profiles", [])
                        self.embibe_token = linked_profiles[0].get("embibe_token")
                        self.userId = linked_profiles[0].get("id")
                        self.email_id = email

                        if linked_profile_id is not None:
                            for linked_profile in linked_profiles:
                                if str(linked_profile.get("id")) == str(linked_profile_id):
                                    self.embibe_token = linked_profile.get("embibe_token")
                                    self.userId = linked_profile.get("id")

                    else:
                        self.embibe_token = response.json().get("data", {}).get("embibe_token")
                        self.userId = response.json().get("data", {}).get("id")
                        self.email_id = email

                return self.embibe_token

            time.sleep(5)

        return None
    
    def teacher_signIn(self, host, email, password):

        for i in range(2):
            headers = {
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                'Origin': 'https://staging-fiber-web.embibe.com',
                'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            }

            if "fiberdemoms" in host:
                headers["Origin"] = "https://fiber-demo-web.embibe.com"
            elif "beta" in host or 'api-prod' in host:
                headers["Origin"] = "https://beta-api.embibe.com"

            mobile = 0
            try:
                mobile = int(email)
            except Exception:
                pass
            
            url = f"{host}/narad/v2/availableOrg?email={email}"

            response = requests.request("GET", url, headers=headers)
            self.org_id = response.json()[0].get("orgId")
            self.org_token = response.json()[0].get("orgToken")
            headers['org-token'] = response.json()[0].get("orgToken")

            payload = {"email": email, "password": password, "device_id": generate_random_str_num(18), "orgId": self.org_id}

            url = f"{host}/user_auth_ms/sign-in"

            api_request_time = datetime.datetime.now().timestamp()
            response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
            api_response_time = datetime.datetime.now().timestamp()
            self.cookies = response.cookies
            self.school_preprod_embibe_token = requests.utils.dict_from_cookiejar(response.cookies).get("school_preprod_embibe-token")

            if response.status_code == 200:
                self.reseller_jwt_token = response.headers["embibe-token"]
                headers['reseller-jwt-token'] = self.reseller_jwt_token

                show_url = f"{host}/narad/v2/show"

                response = requests.request("GET", show_url, headers=headers, data={})

                if response.status_code == 200:
                    self.reseller_jwt_token = response.headers.get("reseller-jwt-token")
                    self.teacher_id = response.json().get("adminId")
                    self.root_org_id = response.json().get("rootOrganizationId") # boards
                    self.school_id = response.json().get("parentOrganizationId") # updateclass
                    if "classDetails" in response.json() and len(response.json().get("classDetails", [])) > 0:
                        class_details = response.json().get("classDetails", [])
                        self.class_id_list = [{"class_id": x.get("classOrgId"), "goal": x.get("board"), "exam": x.get("grade")} for x in class_details]
                        self.class_id = class_details[-1].get("classOrgId")
                        self.class_name = class_details[-1].get("name")
                        self.email_id = email



                return self.reseller_jwt_token, self.school_preprod_embibe_token

        return None


def create_new_user(host):
    url = f"{host}/user_auth_ms/create_user"

    email = f"{generate_random_string(6).lower()}.achieve.june21@embibe.com"
    payload = json.dumps({
        "email": email,
        "first_name": "fname",
        "last_name": "lname",
        "password": "embibe1234",
        "primary_goal": "kve383630",
        "primary_exam": "kve383631",
        "profile_pic": "https://assets.embibe.com/production/web-assets/images/avatar/orange.png",
        "meta": {
            "test": "test"
        }
    })
    headers = {
        'Content-Type': 'application/json',
        'white-list-key': 'aUsb66BbYF6o2XcA5agG',
        'whitelist-source-redirected-url': 'https://beta.embibe.com/education-goa',
        'Origin': 'https://staging-fiber-web.embibe.com',
        'Cookie': 'V_ID=ultimate.2021-05-20.4d3347a882d914c7390cf2bba894233a; __cfduid=dd65b8e235417ded216f1ff0aea8a4fd11620065201; embibe-refresh-token=7b72e547-3030-494d-b102-db0709628ea3; preprod_ab_version=0; preprod_embibe-refresh-token=b5ff46d4-256c-43cb-93e8-fa9886f3ff1e; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2MjE4Mzk3NDUsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMDA0MTIxNywiZXhwIjoxNjIzMDQ5MzQ1LCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoiZmltbGl0ZXN0aUBiaXlhYy5jb20ifQ.FsQuO_YGgJ8g5kgwt6dESfseqPVr3hz_WO7V45ClNaaT7MJZ7ahaNUr8FW_OLyC7A4Mv8OGh8W3_7uA9XJjZDw; prod_embibe-refresh-token=0a273e62-c496-4250-9892-519253000cc7; staging_ab_version=0; staging_embibe-refresh-token=512c7c60-7c9f-4067-b014-a0511dbc218c; staging_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2MjEzNTMwMDcsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMDAyMjQ1NSwiZXhwIjoxNjIyNTYyNjA3LCJkZXZpY2VJZCI6IjE2MjEzNTE4OTY1MjIiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoiYWpheS5yYW53YTJAZW1iaWJlLmNvbSJ9.oJjcVwD4Vbik4-DWc9Mf19Xc-CzSMvl9YKPZ-4q_6XfzT1zFx6S7z59qicrrba4lkF400BGuwfkS0NwCJs-dNw'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    if response.status_code == 201:
        print("User Created")

    print(email, "embibe1234")
    return email, "embibe1234"
