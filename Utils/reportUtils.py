from datetime import date
import pandas as pd
import re


class SEOWarmupReport:

    def categorize_by_level(url: str = ""):
        if "/questions/" in url:
            return "QUESTION"
        elif "/books/" in url:
            if len(url.split("/")) == 6:
                return "BOOK"
            elif len(url.split("/")) == 7:
                return "CHAPTER"
            elif len(url.split("/")) == 8:
                return "EXERCISE"
        return None

    def __init__(self, report) -> None:
        report["level"] = report["url"].apply(__class__.categorize_by_level)
        report = report[["url", "level", "status code"]]
        report.columns.name = "temp_i"
        report.index.name = "Sr No."
        report.columns.name = "Sr No."
        report.index.name = None
        self.report = report
        self.summary = self.create_report_summary()

    def create_report_summary(self):
        report = self.report.copy()
        report.index.name = None
        report.columns.name = None
        report.reset_index(drop=True)
        summary = report.pivot_table(index = "level",columns = "status code", values = "url", aggfunc = "count")
        summary.index.name = "Sr. No."
        summary.columns.name = "Sr. No."
        summary.reset_index(drop=True)
        summary.index.name = None
        return summary

    def create_report_html(self):
        report = self.report
        html = f"""
        
<p>
<h4>Results</h4>
</p>
<p>
{self.summary.to_html()}
</p>
"""
        html = re.sub("\\n","",html)
        return html
