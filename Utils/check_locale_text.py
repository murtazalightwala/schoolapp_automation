import re
import pandas as pd


def removeMathML(text):
    start = "<math xmlns="
    end = "</math>"
    i = 0
    while True:
        i += 1
        if i == 10:
            break

        if text.find(start) == -1 or text.find(end) == -1:
            break

        text = text[:text.find(start)] + text[text.find(end) + len(end):]

    return text


def check_hindi_text(text):
    total_text_len = len(text)
    text = removeMathML(text)
    tt = text.replace("&nbsp;", "").replace("&#39;", "").replace("Embibe", "").replace("embibe", "")
    text = re.sub(r"http\S+", "", text)

    non_hindi_chars = 0
    special_char_and_num = list(range(1, 65)) + list(range(91, 97)) + list(range(123, 128))
    if type(tt) == str and len(tt) > 0:
        for ch in tt:
            if not (2304 <= ord(ch) <= 2431) and (ord(ch) not in special_char_and_num):
                non_hindi_chars += 1

        fail_thershold = (non_hindi_chars * 100) / total_text_len
        if fail_thershold > 25:
            if fail_thershold < 50:
                df = pd.read_csv("failed_hindi_cases.csv")
                df.loc[len(df)] = [text, fail_thershold]
                df.to_csv("failed_hindi_cases.csv", index=False)

            return False

        return True

    return False


def check_non_en_text(text, uni_start, uni_end):
    total_text_len = len(text)
    text = removeMathML(text)
    tt = text.replace("&nbsp;", "").replace("&#39;", "").replace("Embibe", "").replace("embibe", "")
    text = re.sub(r"http\S+", "", text)

    if type(tt) == str and len(text) < 5:
        return True

    non_hindi_chars = 0
    special_char_and_num = list(range(1, 65)) + list(range(91, 97)) + list(range(123, 128))
    if type(tt) == str and len(tt) > 0:
        for ch in tt:
            if not (uni_start <= ord(ch) <= uni_end) and (ord(ch) not in special_char_and_num):
                non_hindi_chars += 1

        fail_thershold = (non_hindi_chars * 100) / total_text_len
        if fail_thershold > 25:
            if fail_thershold < 50:
                df = pd.read_csv("failed_hindi_cases.csv")
                df.loc[len(df)] = [text, fail_thershold]
                df.to_csv("failed_hindi_cases.csv", index=False)

            return False

        return True

    return False


def check_locale_text(text, locale):
    if locale == "en":
        return True
    elif locale == "hi" or locale == "mr":
        return check_non_en_text(text, 2304, 2431)
    elif locale == "te": # Telgu
        return check_non_en_text(text, 3072, 3199)
    elif locale == "kn": #Kannada
        return check_non_en_text(text, 3200, 3327)
    elif locale == "ta": # Tamil
        return check_non_en_text(text, 2944, 3071)
    elif locale == "ml": # Kerala - Malayalam
        return check_non_en_text(text, 3328, 3455)
    elif locale == "pa": # Punjabi - Gurmukhi
        return check_non_en_text(text, 2560, 2687)
    elif locale == "as" or locale == "bn" : # Assam - Bengal
        return check_non_en_text(text, 2432, 2559)
    elif locale == "gu": # Gujarat Board
        return check_non_en_text(text, 2688, 2815)
    elif locale == "or": # Odisha Board
        return check_non_en_text(text, 2816, 2943)
