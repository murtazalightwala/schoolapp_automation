import os

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import gspread_dataframe as gd
import time
import traceback

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

creds = ServiceAccountCredentials.from_json_keyfile_name('apiautomation-5f15e583dede.json', scope)
client = gspread.authorize(creds)

gc = gspread.service_account(filename='apiautomation-5f15e583dede.json')


def get_sheet_to_df_with_link(url):
    time.sleep(5)
    for i in range(5):
        try:
            sheet = gc.open_by_url(url)
            worksheet = sheet.get_worksheet(0)

            return pd.DataFrame(worksheet.get_all_records())
        except Exception as e:
            print(traceback.format_exc())
            time.sleep(102)


def update_sheet_by_df(file_id, df, wk_name):
    print(wk_name)
    for i in range(5):
        try:
            sheet = gc.open_by_key(file_id)
            worksheet = sheet.worksheet(wk_name)

            worksheet.clear()
            gd.set_with_dataframe(worksheet, df)
            return
        except Exception:
            print(file_id)
            print(wk_name)
            print(traceback.format_exc())
            time.sleep(102)


def get_sheet_to_df(file_id, worksheet_name):
    time.sleep(5)
    print(worksheet_name)
    for i in range(5):
        try:
            sheet = gc.open_by_key(file_id)
            worksheet = sheet.worksheet(worksheet_name)

            return pd.DataFrame(worksheet.get_all_records())
        except Exception as e:
            print(file_id)
            print(worksheet_name)
            print(traceback.format_exc())
            time.sleep(102)

def getAllWorksheets(file_id):
    sheet = gc.open_by_key(file_id)
    worksheet_list = sheet.worksheets()
    worksheet_list = [wk.title for wk in worksheet_list]
    return  worksheet_list

def getAllRecords(key, worksheet_name):
    print(key, worksheet_name)
    time.sleep(5)
    sheet = gc.open_by_key(key)
    worksheet = sheet.worksheet(worksheet_name)
    return worksheet.get_all_records()
