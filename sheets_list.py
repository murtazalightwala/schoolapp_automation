book_topic_sheets = {
    "en": "1AWuEWAZ4Ny1S8E_yvNfo0MFc2eeNTayLsczXyWGzhks",
    "gu": "1ZkpR2yEHxb-CEbDiobvn0cMOfawCmKYTs9SjToVHsLc",
    "as": "16qapYTrqCBYCt5kV8mFYZ_nUI4F1noX0RNaiQR3EG38",
    "mr": "1o-7XmCLx7xkNXzV-dD9j8vQdqLZQvhGKzXfXhrSGeco",
    "te": "19kWXdEFuX-xFEjWS0dP1g7M4vhaTlO7QaaSKUxaN8uU",
    "ta": "1FtEMqqD9l4FUI4qrOlP7tSuCZXa3lf5nwp07cQKiKXw",
    "kn": "1USIPPPxcYeW2Zg_EnmOCBxSAwQSMcx8BbwaROsvWElo",
    "ml": "1-6XwjDz2XIqQMvZibM0pDCDaH3D9XMwVOkr-ruhsQX8",
    "or": "1-K6f2Tt9mbTv3tnaL8O9ZN6xNCkFEjqVfzbWC3lZM44",
    "pa": "1QPBXBKe_DoxYgWVMD1VoxE0x-ipIwct66gob_qcfpcQ",
    "hi": "1t6USM1UO5S0uXEKbqwoUGBTfj048NF9sFyk41rzpx6w"

}

results_sheets = {
    "en": "1Wvq_vQu2BkLUS83s4tScWj7mW8NL_DKo1KRSeVwYgK8",
    "gu": "11dZMHHXj-Q5B20dGJYCFyOOfge6rnNJ337xE4UTeyBY",
    "as": "19v9xApIOSQE7-zIGMRUz1qCDuKG7fKy12V5mOvONQHk",
    "mr": "1-Q7giJz0YgTHMR-4GoFF4FUCiqAEYG718nEkXARqT7I",
    "te": "1U5z9qPAvQZ91izsMyEVzjqvrqfANpi63CmAqlRgrb9E",
    "ta": "18cZER-ZrStn-8UR2OkcM3RSIwryK6LCHuNnkjpxh6bc",
    "kn": "1J1sO2nXP8vDyG02mp3WSiiHKGOMEtSmzRlWQTT70qbI",
    "ml": "19nmoNYaJmiF0Yw46Q6BH0jMRW6Sq7s6_hkneAgbd4to",
    "or": "1un45MXj56J0JCFkJ_CnoD-u_7TOjG2RB9mmpm7nf5hE",
    "pa": "19qmDfTAxNIwaelVLaa8V8dHDxzER9EzZeMMciRRIdI0",
    "hi": "1TiAYpgoVJ0QQ8G6siBIkGJnGHpK1OcWgAshP1Q0pkN8"
}

check_books_sheets = {
    "en": "1dSDZUzE4jlJ6koK6gCrvHan7_4VuJssHKUykkxWyhBM"
}

big_book_topics_sheets = {
    "en": "1gwzfa2X3cM3DxKepw3aj0DZgQBTyoQLkPXJWmfo1UFY",
    "gu": "1VL4G6zorkXNA8TGB6nUwP49w_d6tUGO_eDNyh7j8kaA",
    "as": "1vDCCrjJuNp5kB8IcdG6fc9BofUO8AfdhCVvt1LZABKg",
    "mr": "1yT1bH-fVOtIuLtgiqbXIOpR5ur4uDrG_7KIO_F9Dt0U",
    "te": "1pwqCPHxSRsemQ_8flTbyhS809YaFUsggyk4_elRBHSo",
    "ta": "1hKTVyxZ_dZNzheyR1hP1uYiOsNSdVVJQOGXSwq7mFks",
    "kn": "1raB-8ZVDKujKocU7DY7A0KGDGvcYBCkdB92i1XIRCIM",
    "ml": "1pWgLJmULbACb8ohrKBo-DUUycIToGKcpqAqlpz4OMJ0",
    "or": "1TcL2Ie1MK5egAySOjDP3i_hKiQv6ejGU0QeDY7WYqsI",
    "pa": "1mF44nIuwjfZb18X8lNQ0p0i1nHXkOhZ1KitfUj8IVcE",
    "hi": "1KnI0iJWeixvdKcZzuqClMNCVzZa6NmndsXbP5VwT6VY"

}

prerequisite_readiness_results = {
    "en": "1fxRgMx8gv-TpJWBn-0Qew3qfi2eOxXwBKJKJu-xuOPU",
    "gu": "1kCYsSYEurFhQK60CZgdcIX1noVHg3wQ-8VC_UFnU0Tk",
    "as": "19ARPSSqAge8npcNh90A-CIgLJlkOJMcLhYlBQV9j5kE",
    "mr": "1EW5ewQkbh0VClK39k16t8c6z8--3TB3UBUz7iTJdeqM",
    "te": "1mKp4zKa6cZPgN4JqRy-6wEHYm7Jr527XM9eVU8SWSlg",
    "ta": "1MVVh0IPdO3Y_v1beMddVLMQB13dwGJ7QNfawpD2r56E",
    "kn": "1zOMBvk3xdopAlzZ3NRjQm7dtSn9sX12MzcHl25emuoM",
    "ml": "1wWYfdat6Z2UJmKxVhrCQFh1s8ulF26QG5S5I7x8HqrU",
    "or": "1IxqHhhhaJPAJWfEefoHDIEq-usD3s5Pl0oGTAWgJQdc",
    "pa": "1bs64ZRnkutVMUFLrz2J10LYFk-u6fLKvG5kqWvv8SrU",
    "hi": "14QmMRr-udUppxI2Wl0sgHTUQqqqmMmp7GulsjI4w1jY"
}

book_ops_results = "1dSDZUzE4jlJ6koK6gCrvHan7_4VuJssHKUykkxWyhBM"

recap_suggestions_results = {
   "en": "1e9KeaaQbyFApmf5zS5cRFyAYF4Y9TWM-WuKFU2Qh1h0",
    "gu": "1i_tqi8RNh-AE7zpfo2NaTGDO_W0BFv2tXMG34S7m_iw",
    "as": "1W7I7z-2mFbjoIq37ElJBy0j8nVb_ILdy31vEWAxPjYU",
    "mr": "195o0ATig2OQRtXS-p0i3-Da0-tZgykJ6eZMwNKeYHH4",
    "te": "1jbOdePLLQPmb3sU5F_YN7QHKMDPlZZJvOguo1QsQZJo",
    "ta": "1Lj05jBQZlQ83nn5X5ddsyy1Yah5LodhcaoIXLhaAPgo",
    "kn": "14OYX9kHXXl3oOLVh7r30fI76XYMuq_Pelk7M5pHVkRg",
    "ml": "1mJPSZ9Jv_4QbUYP5qwGGXDRl89TpG7s8Xhua-hvY2n0",
    "or": "1mmLD1RvHdWCPa4hqa4lOlcFnk5Mo8mBQIc2ZA_KT1fY",
    "pa": "1s57PArzDE0_oN5weuRa2VmeNmdv6J_mQQr3_kiQt_8U",
    "hi": "1l0Kr19mYbpZddDG2pE7PGTTfNPxBVIeLQHwT_tPiOUU" 
}

fs_ms_search_results = {
    "en": "1IbU6GKK1SV7cpUQkbwmLL53nPklHd7uzJiSVTodXdE4",
    "gu": "1Ij5HiaDNEpBxeiCLjiK9a7QhHXYz_-Y_XzNFfQVaArI",
    "as": "1RF6I2jSoRRO7EsdGc8reF79XTqqgR2FRa6L0rnMXcWw",
    "mr": "1fAEbg9GHD92hXyCsEMgzUmI4T7nQQQLPzgr7b2T9pUQ",
    "te": "1cVu3lL9WeqXCz5EQmZ06_d4qOa3_NI6ZO2G_tq2cJSk",
    "ta": "1Jje_6Ppl838Zwwge5tywmbTaxDdI8MvB-YyNA6vArA4",
    "kn": "1s7aEDrV2CkkB1VEeLPtcNIRRsZ63B8B2pztOKw-fTPc",
    "ml": "1ShZjknZ1xCSH1hMx2RqsoNre6sGE-lp6Kk16v7CMkG8",
    "or": "1kF_RgCcczn14MyZ79DjOVTAfxOumcTij4ZFl2_bOKhw",
    "pa": "1S_6ETkpGB9MXTZDJaTtq-IdveVmbo3HFy8J6UH2rOVU",
    "hi": "1g8veOJNn4jOkVJhb9Rn9mH3afOlYCoxCbw7gz6kpyvA" 
}