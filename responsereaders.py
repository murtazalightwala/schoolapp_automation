import regex as re
from Utils.callAPI import callAPI


class ResponseReader:
    def __init__(self, response):
        self.response = response
        if response.status_code not in [200, 201, 202, 204]:
            try:
                self.message = response.json().get("message")
            except:
                print("No error message",response.status_code)

    
    def get_status_code(self):
        return self. response.status_code
    
    def get_method(self):
        return self.response.request.method
    
    def get_error_message(self):
        return getattr(self, "message", "NA")

class FiberCountriesResponseReader(ResponseReader):
    def __init__(self, response):
        self.response = response
        if "fiber-countries-goals-exams" not in response.url:
            self.message = "The response object is not of fiber countries API ..."
        if response.status_code not in [200, 201, 202]:
            self.message = self.response.reason
        if self.response.json().get('success'):
            self.data = self.response.json().get('data',[])
            self.message = "Success!!!"
        else:
            self.message = "Something went wrong!!!"
        print(self.message)

    def get_all_goal_exam_locales(self):
        goal_exam_locale_list = []
        
        for goal_meta in self.data:
            
            for exam_meta in goal_meta.get('exam',[]):

                for locale in exam_meta.get('supported_languages',[]):

                    goal_exam_locale_entry = {}
                    goal_exam_locale_entry['goal_name'] = goal_meta.get('name')
                    goal_exam_locale_entry['goal_code'] = goal_meta.get('code')
                    goal_exam_locale_entry['goal_format_reference'] = goal_meta.get('format_reference')
                    goal_exam_locale_entry['vertical'] = goal_meta.get('type')
                    goal_exam_locale_entry['exam_name'] = exam_meta.get('name')
                    goal_exam_locale_entry['exam_code'] = exam_meta.get('code')
                    goal_exam_locale_entry['exam_format_reference'] = exam_meta.get('format_reference')
                    goal_exam_locale_entry['grade'] = exam_meta.get('grade')
                    goal_exam_locale_entry['exam_slug'] = goal_meta.get('slug')
                    goal_exam_locale_entry['locale'] = locale
                    goal_exam_locale_entry['lge_slug'] = "--".join([locale, goal_exam_locale_entry['goal_name'], goal_exam_locale_entry['exam_name']])
                    goal_exam_locale_list.append(goal_exam_locale_entry)
        return goal_exam_locale_list

class ATGConfigResponseReader(ResponseReader):
    def __init__(self, response):
        self.response = response
        self.data = {}
        if response.status_code not in [200, 201, 202]:
            try:
                self.message = self.response.json().get("message")
            except:
                self.message = self.response.text
        else:
            try:
                self.data = self.response.json()
                self.message = "Success!!!"
            except Exception as e:
                self.message = "Something went wrong!!! " + str(e)
    
    def get_atg_config_id(self):
        if self.response.status_code != 200:
            return "Not created"
        return self.data.get('data').get('id')

class ATGConfigGetQuestionsResponseReader(ResponseReader):
    def get_questions(self):
        if self.response.status_code != 200:
            return []
        return [x.get('id') for x in self.response.json().get('data',{}).get('data',[])]

class ATGConfigSetMetaResponseReader(ResponseReader):
    def __init__(self, response):
        self.response = response
        self.data = {}
        if response.status_code not in [200, 201, 202]:
            self.message = self.response.reason
        else:
            try:
                self.data = self.response.json()
                self.message = "Success!!!"
            except Exception as e:
                self.message = "Something went wrong!!! " + str(e)

class ATGConfigPublishResponseReader(ResponseReader):
    def __init__(self, response):
        self.response = response
        self.data = {}
        if response.status_code not in [200, 201, 202]:
            self.message = self.response.reason
        else:
            try:
                self.data = self.response.json()
                self.message = "Success!!!"
            except Exception as e:
                self.message = "Something went wrong!!! " + str(e)
    
    def get_learning_bundle_id(self):
        if self.response.status_code != 200:
            return "NA"
        return self.data.get('data').get('learningBundleId')

class HomeResponseReader(ResponseReader):
    def __init__(self, response):
        self.response= response
        self.data = []
        if "home/" not in response.url:
            self.message = "The response object is not of home API ..."
        if response.status_code not in [200, 201, 202]:
            self.message = self.response.reason
        else:
            try:
                self.data = self.response.json()
                self.message = "Success!!!"
            except Exception as e:
                self.message = "Something went wrong!!! " + str(e)
        print(self.message)

    def get_content_section_types(self):
        content_section_types = []
        for section in self.data:
            if section.get("content_section_type") not in content_section_types:
                content_section_types.append(section.get("content_section_type"))
        return content_section_types

    def get_all_subjects(self):
        subjects = []
        for section in self.data:
            if section['content_section_type'] == "SUBJECTS":
                for entry in section['content']:
                    subjects.append({'subject':entry.get('subject'), 'title':entry['title'].replace("‌", "")})
        return subjects
    
    def get_sections_grouped_by_content_type(self):
        content_type_groups = {}

        for section in self.data:
            if section['contentType'] not in content_type_groups:
                content_type_groups[section['contentType']] = []
            content_type_groups[section['contentType']].append(section)
        return content_type_groups

    def get_all_books(self,**kwargs):      
        content_type_groups = self.get_sections_grouped_by_content_type()
        book_sections = content_type_groups.get('Book',[])

        book_list = []
        for section in book_sections:
            if section.get('size') == section.get("total"):
                book_list.extend(section.get('content', []))
                continue

            payload = {"child_id": kwargs['child_id'], "exam_name": kwargs['exam_name'], "goal": kwargs['goal_name'], "grade": kwargs['grade'],
                "content_section_type": section.get('content_section_type'), "offset": 0, "size": section.get('total'), "locale": kwargs['locale']}

            section_api_response = callAPI(f'/fiber_ms/v1/home/sections', payload, 'POST')
            print(section_api_response.status_code)
            if section_api_response.status_code == 200:
                book_list.extend(section_api_response.json()[0].get('content',[]))
            else:
                print("Something Went Wrong!!!")
        
        return book_list

    def get_all_books_list(self, **kwargs):
        content_type_groups = self.get_sections_grouped_by_content_type()
        book_sections = content_type_groups.get('Book',[])

        book_list = []
        for section in book_sections:
            if section.get('size') == section.get("total"):
                for book_json in section.get('content',[]):
                    book_list.append(book_json.get("book_code",""))
                
                continue

            payload = {"child_id": kwargs['child_id'], "exam_name": kwargs['exam_name'], "goal": kwargs['goal_name'], "grade": kwargs['grade'],
                "content_section_type": section.get('content_section_type'), "offset": 0, "size": section.get('total'), "locale": kwargs['locale']}

            section_api_response = callAPI(f'/fiber_ms/v1/home/sections', payload, 'POST')
            print(section_api_response.status_code)
            if section_api_response.status_code == 200:
                for book_json in section_api_response.json()[0].get('content',[]):
                    book_list.append(book_json.get("book_code",""))
                
            else:
                print("Something Went Wrong!!!")
        
        return book_list

    def get_book_by_book_code(self, **kwargs):
        for book in self.get_all_books(**kwargs):
            if book['book_code'] == kwargs['book']:
                return book
        return {}
    
    def get_book_position_in_api_response(self, **kwargs):
        i = 1
        for book in self.get_all_books(**kwargs):
            if book['book_code'] == kwargs['book']:
                return i
            i+=1
        return -1    

    def get_all_videos(self,**kwargs):      
        content_type_groups = self.get_sections_grouped_by_content_type()
        video_sections = content_type_groups.get('Video', [])

        video_list = []
        for section in video_sections:
            if section.get('size') == section.get("total"):
                video_list.extend(section.get('content', []))
                continue

            payload = {"child_id": kwargs['child_id'], "exam_name": kwargs['exam_name'], "goal": kwargs['goal_name'], "grade": kwargs['grade'],
                "content_section_type": section.get('content_section_type'), "offset": 0, "size": section.get('total'), "locale": kwargs['locale']}

            section_api_response = callAPI(f'/fiber_ms/v1/home/sections', payload, 'POST')
            print(section_api_response.status_code)
            if section_api_response.status_code == 200:
                video_list.extend(section_api_response.json()[0].get('content',[]))
            else:
                print("Something Went Wrong!!!")
        
        return video_list

    def get_all_tests(self,**kwargs):      
        content_type_groups = self.get_sections_grouped_by_content_type()
        test_sections = content_type_groups.get('Test', [])

        test_list = []
        for section in test_sections:
            if len(section.get('content')) == section.get("total"):
                test_list.extend(section.get('content', []))
                continue

            payload = {"child_id": kwargs['child_id'], "exam_name": kwargs['exam_name'], "goal": kwargs['goal_name'], "grade": kwargs['grade'],
                "content_section_type": section.get('content_section_type'), "offset": 0, "size": section.get('total'), "locale": kwargs['locale']}

            section_api_response = callAPI(f'/fiber_ms/v1/home/sections', payload, 'POST')
            print(section_api_response.status_code)
            if section_api_response.status_code == 200:
                test_list.extend(section_api_response.json()[0].get('content',[]))
            else:
                print("Something Went Wrong!!!")
        
        return test_list

    def get_tests_segregated_by_type(self, **kwargs):
        test_groups = {}
        test_groups['chapter_test'] = []
        test_groups['full_test'] = []
        test_groups['part_test'] = []
        test_groups['previous_year_test'] = []

        content_type_groups = self.get_sections_grouped_by_content_type()
        test_sections = content_type_groups.get('Test', [])

        for section in test_sections:
            if "CHAPTERWISE" in section.get('content_section_type'):
                key = "chapter_test"
            if "FULL" in section.get('content_section_type'):
                key = "full_test"
            if "PART" in section.get('content_section_type'):
                key = "part_test"
            if "PREVIOUS_YEAR" in section.get('content_section_type'):
                key = "previous_year_test"

            if len(section.get('content')) == section.get("total"):
                test_groups[key].extend(section.get('content', []))
                continue

            payload = {"child_id": kwargs['child_id'], "exam_name": kwargs['exam_name'], "goal": kwargs['goal_name'], "grade": kwargs['grade'],
                "content_section_type": section.get('content_section_type'), "offset": 0, "size": section.get('total'), "locale": kwargs['locale']}

            section_api_response = callAPI(f'/fiber_ms/v1/home/sections', payload, 'POST')
            print(section_api_response.status_code)
            if section_api_response.status_code == 200:
                test_groups[key].extend(section_api_response.json()[0].get('content',[]))
            else:
                print("Something Went Wrong!!!")
        return test_groups

    def get_test_by_mb_code(self, **kwargs):
        for test in self.data:
            if test['bundle_id'] == kwargs['test']:
                return test
        return {}

class HomeSubjectResponseReader(HomeResponseReader):
    def __init__(self, response):
        self.response = response
        self.subject = self.get_subject()
        self.data = []
        
        if "home/" not in response.url:
            self.message = "The response object is not of home API ..."
        if response.status_code not in [200, 201, 202]:
            self.message = self.response.reason
        else:
            try:
                self.data = self.response.json()
                self.message = "Success!!!"
            except Exception as e:
                self.message = "Something went wrong!!! " + str(e)
        print(self.message)

   
    def get_subject(self):
        match_object = re.search("(?<=/home/\w+/)(?P<subject>.+)(\?)" , self.response.url)
        return match_object.group("subject")

class UserHomeResponseReader(ResponseReader):
    def get_all_school_assignments(self):
        if self.get_status_code().split(":")[0] != "200":
            return []
        
        for carousel in self.response.json().get("carousels"):
            if carousel.get("type") == "SchoolAssignments":
                return carousel.get("data", [])
    
    def get_assignment_list(self):
        assignments = []
        try:
            assignments.extend(self.get_all_school_assignments())
        except:
            pass
        return [x.get("schoolAssignmentId") for x in assignments]
        
class GetSyllabusResponseReader(ResponseReader):
    pass

class GetChapterTopicResponseReader(ResponseReader):
    pass

class GetBooksContentResponseReader(ResponseReader):
    def get_book_list(self):
        if self.get_status_code() != 200:
            return []
        return self.response.json().get("data").get("bookList")

    def get_active_book(self, topic):
        book_list = self.get_book_list()
        if len(book_list) == 0:
            return {}
        for book in book_list:
            # if book.get("totalQuestionsInLessonTopics") == 0:
            #     continue
            if book.get("bookPath") == topic.get("path"):
                return book
        print("Book not found in list")
        return book_list[0]

class SetActivityContentResponseReader(ResponseReader):
    def get_question_count(self):
        if self.get_status_code == 200:
            return self.response.json().get("data").get("questionCount")
        return 0

class ConfirmSyllabusResponseReader(ResponseReader):
    pass

class GetHomeworkChaptersResponseReader(ResponseReader):
    pass

class GetHomeworkTopicsResponseReader(ResponseReader):
    pass

class GetTopicVideosResponseReader(ResponseReader):
    def get_video_count(self):
        if self.response.status_code != 200:
            return 0
        return self.response.json().get("data").get("pagination").get("count")
    
    def get_video_codes(self):
        if self.response.status_code != 200:
            return []
        video_data = self.response.json().get("data").get("topicVideos")
        video_list = [video["id"] for video in video_data]
        return video_list
        
class GetTopicQuestionsResponseReader(ResponseReader):
    def get_question_count(self):
        if self.response.status_code != 200:
            return 0
        return self.response.json().get("data").get("metaData").get("records")
    
    def get_question_codes(self):
        if self.response.status_code != 200:
            return []
        question_data = self.response.json().get("data").get("data")
        question_list = [question["id"] for question in question_data]
        return question_list
        
class GetAllTestsResponseReader(ResponseReader):
    def get_test_count(self):
        if self.get_status_code() != 200:
            return 0
        return len(self.response.json().get("data").get("testList"))

    def get_test(self):
        if self.get_status_code() != 200:
            return {}
        test_list = self.response.json().get("data").get("testList")
        if len(test_list) == 0:
            return {}
        return test_list[0]
    
class ChaptersResponseReader(ResponseReader):
    pass

class TestQuestionsResponseReader(ResponseReader):
    pass

class SelectTestResponseReader(ResponseReader):
    pass

class ReviewQuestionsResponseReader(ResponseReader):
    pass

class SetVideosResponseReader(ResponseReader):
    pass
class SetQuestionsResponseReader(ResponseReader):
    pass

class SearchQuestionsResponseReader(ResponseReader):
    
    def get_number_of_questions(self):
        if self.get_status_code() != 200:
            return 0
        return self.response.json().get("results")[0].get("no_of_results")
    
class BooksandQuestionsResponseReader(ResponseReader):
    def get_number_of_questions(self, type):
        if self.get_status_code() != 200:
            return 0
        return self.response.json().get(type, 0)

class PreRequisiteResponseReader(ResponseReader):
    def get_total_videos(self):
        if self.get_status_code() != 200:
            return 0
        data = self.response.json()
        video_count = 0
        for object in data.get("prerequisiteTopics"):
            video_count+=len(object.get("videos"))
        return video_count
    
    def get_prerequisite_topics(self):
        if self.get_status_code() != 200:
            return []
        data = self.response.json()
        topics = []
        for object in data.get("prerequisiteTopics"):
            topics.append(object.get("topicKvCode"))
        return topics

class ShowResponseReader(ResponseReader):
    pass

class BookResponseReader(ResponseReader):
    def get_book_list(self, type = "Book"):
        if self.get_status_code() != 200:
            return []
        book_list = self.response.json().get("data")
        for book in book_list:
            book["type"] = type
        return book_list

class BookOpsResponseReader(ResponseReader):
    def is_updated(self):
        return self.get_status_code() == 200
    
    def get_error_message(self):
        if self.get_status_code() == 200:
            return "NA"
        else:
            return self.response.json().get("message")
    
    def get_confirmation(self):
        if self.get_status_code() != 200:
            return self.get_error_message()
        return self.response.json().get("description")

class LearningMapsFormatsResponseReader(ResponseReader):

    def get_book_list(self):
        if self.get_status_code() != 200:
            return []
        return self.response.json().get("_items")

class SubjectsResponseReader(ResponseReader):

    def get_all_subjects(self):
        subject_list = []
        if self.get_status_code() != 200:
            return []
        for entry in self.response.json():
            class_meta = {}
            class_meta["exam_id"] = entry.get("id")
            class_meta["class_org_id"] = entry.get("classOrgId")
            class_meta["board"] = entry.get("board")
            class_meta["grade"] = entry.get("grade")
            class_meta["goal_code"] = entry.get("goalCode")
            class_meta["exam_code"] = entry.get("examCode")
            for subject in entry.get("subjects", []):
                subject_meta = {}
                subject_meta["subject_id"] = subject.get("id")
                subject_meta["subject_name"] = subject.get("name")
                subject_meta["book_list"] = subject.get("bookLists")
                subject_meta["big_book_list"] = subject.get("bigBooks")
                subject_meta["subject_code"] = subject.get("subjectCode")
                subject_list.append({**class_meta, **subject_meta})
        return subject_list
        
class StudentPrerequisiteResponseReader(ResponseReader):
    def get_all_topics(self):
        topic_list = []
        if self.get_status_code() != 200:
            return []
        for topic in self.response.json():
            topic_list.append(topic)
        return topic_list
    
    def get_all_topic_codes(self):
        topic_list = []
        if self.get_status_code() != 200:
            return []
        for topic in self.response.json():
            topic_list.append(topic.get("title"))
        return topic_list

class StudentTopicsVideosResponseReader(ResponseReader):
    def get_video_count(self):
        video_count = 0
        if self.get_status_code() != 200:
            return video_count
        for section in self.response.json():
            for subsections in section.get("sections"):
                video_count+=len(subsections.get("content"))
        return video_count
                
class FiberMSBookResponseReader(ResponseReader):
    def get_book_kv_path(self):
        return self.response.json().get("data").get("kv_path")

class FiberAppLMFResponseReader(ResponseReader):
    def get_video_count(self, topic):
        if self.get_status_code() != 200:
            return -1
        for _topic in self.response.json().get("Topic"):
            if _topic.get("code") == topic.get("topic_code"):
                return _topic.get("videos_count")
    
    def get_question_count(self, topic):
        if self.get_status_code() != 200:
            return -1
        for _topic in self.response.json().get("Topic"):
            if _topic.get("code") == topic.get("topic_code"):
                return _topic.get("practice_count")

class RecapSuggestionsResponseReader(ResponseReader):
    pass

class FSMSSearchResponseReader(ResponseReader):
    def get_content_count(self):
        if self.get_status_code() != 200:
            return -1
        count = 0
        for result in self.response.json().get("results"):
            count+= result.get("no_of_results")
        return count

class FSMSAMPSearchResponseReader(ResponseReader):
    def get_content_count(self):
        if self.get_status_code() != 200:
            return -1
        return len(self.response.json().get("content", []))

class PrebuiltLessonsResponseReader(ResponseReader):
    def get_prebuilt_lessons_count(self):
        if self.get_status_code() != 200:
            return 0
        return len(self.response.json())
        