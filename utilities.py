from Utils.sheetUtils import getAllRecords
from Utils.db import QADb
import Utils.globalVar as gv
from sheets_list import book_topic_sheets, big_book_topics_sheets
import pandas as pd

def get_all_chapter_topics():
    return getAllRecords(book_topic_sheets.get(gv.local_to_check),"Topics")
    
def get_failed_chapter_topics():
    return getAllRecords(book_topic_sheets.get(gv.local_to_check),"FailedTopics")


def get_verticalwise_book_topics(vertical = None, goal = None, exam = None):
    db = QADb()
    query = {
        "type": "book"
    }
    if gv.local_to_check:
        query["locale"] = gv.local_to_check
    if vertical != None:
        query["vertical"] = vertical
    if goal != None:
        query["goal"] = goal
    if exam != None:
        query["exam"] = exam
    cursor = db.topic_list.find(query)
    results = []
    for doc in cursor:
        results.append(doc)
    # all_chapters_list = getAllRecords(book_topic_sheets.get(gv.local_to_check),"Topics")
    # # with open( "topic.p", "rb" ) as f:
    # #     kwargs['all_chapters_list'] = pickle.load(f)
    # df = pd.DataFrame(all_chapters_list)
    # if vertical:
    #     df = df[df["vertical"] == vertical]
    # return df.to_dict('records')
    return results

def get_verticalwise_big_book_topics(vertical = None, goal = None, exam = None):
    db = QADb()
    query = {
        "type": "big_book"
    }
    if gv.local_to_check:
        query["locale"] = gv.local_to_check
    if vertical != None:
        query["vertical"] = vertical
    if goal != None:
        query["goal"] = goal
    if exam != None:
        query["exam"] = exam
    cursor = db.topic_list.find(query)
    results = []
    for doc in cursor:
        results.append(doc)
    return results


    # all_chapters_list = getAllRecords(big_book_topics_sheets.get(gv.local_to_check),"Topics")
    # # with open( "topic.p", "rb" ) as f:
    # #     kwargs['all_chapters_list'] = pickle.load(f)
    # df = pd.DataFrame(all_chapters_list)
    # if vertical:
    #     df = df[df["vertical"] == vertical]
    # return df.to_dict('records')



def setup_gv():
    gv.initGlobalVars()
    gv.initUserMeta()
    
def setup_kwargs(kwargs):
    kwargs["class_name"] = gv.class_name
    kwargs["slot_id"] = "6222034009a8894bfaafa810"
    kwargs["class_id"] = gv.class_id
    kwargs["teacher_id"] = gv.teacher_id
    kwargs["school_id"] = gv.school_id
    kwargs["vertical"] = gv.vertical
    kwargs["timetable_id"] = "62419d625fe43223deb8a3dd"
    kwargs["template_id"] = "61c38789acb4f1190e9950a6"

def setup_chapter_kwargs(kwargs, chapter_data):
    print("Setting up chapter kwargs!!!")
    kwargs['chapter_report'] = {}
    kwargs['chapter_report']['goal'] = chapter_data['goal']
    kwargs['chapter_report']['exam'] = chapter_data['exam']
    kwargs['chapter_report']['book'] = chapter_data['book_name']
    kwargs["topic_name"] = chapter_data['topic_name']
    kwargs['chapter_report']['chapter_name'] = chapter_data.get('chapter_name')
    kwargs['chapter_report']['topic_name'] =  chapter_data['topic_name']
    kwargs["lm_meta_display_name"] = "static"
    kwargs["topic_code"] = chapter_data['topic_code']
    kwargs["chapter_name"] = chapter_data['chapter_name']
    kwargs["book_path"] = chapter_data['path']
    kwargs["book_learning_path_format_name"] = "static"
    kwargs["subject_code"] = chapter_data['subject_code']
    kwargs["subject_id"] = chapter_data['subject_id']
    kwargs["chapter_code"] = chapter_data['chapter_code']
    kwargs["subject_name"] = chapter_data['subject']
    print("Set up chapter kwargs done!!!")

def get_class_id(topic):
    goal = topic.get("goal")
    exam = topic.get("exam")
    for _class in gv.class_id_list:
        if _class.get("goal") == goal and _class.get("exam") == exam:
            return _class.get("class_id")
    return _class.get("class_id")

def get_all_queue_result(queue):
    result_list = []
    while not queue.empty():
        result_list.append(queue.get())
    return result_list
